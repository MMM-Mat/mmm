// webpack.config.js
const path = require('path');

module.exports = {
  // Set the entry point for your renderer process code
  entry: './src/renderer/index.js', // Example entry point
  
  // Specify the output configuration for bundled files
  output: {
    path: path.resolve(__dirname, 'dist'), // Output directory
    filename: 'bundle.js', // Output filename
  },
  
  // Add module rules for processing different file types (e.g., JavaScript, CSS)
  module: {
    rules: [
      // Add rules for processing JavaScript, CSS, etc.
    ],
  },

  // Exclude the 'public' folder from Webpack's processing
  // This prevents Webpack from bundling main process files
  // Note: 'node_modules' is typically excluded by default
  // You can add other folders to exclude as needed
  resolve: {
    alias: {
      'public': path.resolve(__dirname, 'public'), // Alias for the 'public' folder
    },
  },
};
