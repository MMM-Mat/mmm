import React, { useCallback, useContext, useEffect } from "react";
import { getConnectedEdges, useReactFlow } from "reactflow";
// Contexts
import { WidgetsContext } from "../Contexts/WidgetsContextProvider";
// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleInfo, faTrash, faPen, faEyeSlash, faCopy } from '@fortawesome/free-solid-svg-icons'
// Utils
import {  edgeIDtoEdgenodeID, edgenodeIDtoEdgeID, mmmidToContentID } from "../Utils/SomeFunctions";
import { getParents } from "../Utils/algo";

export default function ContextMenu(argprops) {
  const { id,
    top,
    left,
    right,
    bottom,
    ...props } = argprops;
  const landmark = props.data;
  const { getNodes, getEdges, getNode, setNodes, addNodes, setEdges } = useReactFlow();
  const reactflow = useReactFlow();

  // DUPLICATE
  const duplicateLandmark = useCallback(() => {
    const node = getNode(id);
    const landmarkcopy = JSON.parse(JSON.stringify(landmark));
    const newid = node.id + "-copy-" + Date.now();
    landmarkcopy.mmmid = newid;
    const newX = node.position.x + 50;
    const newY = node.position.y + 50;
    const position = {
      x: newX,
      y: newY,
    };
    addNodes({ ...node, id: newid, position, data: landmarkcopy });
  }, [id, landmark, getNode, addNodes]);

  // DELETE
  const deleteLandmark = useCallback(() => {
    const node = getNode(id);
    const parentIDs = getParents(id, getNodes()).map((p) => p.id);
    const aschild = parentIDs.map((pid) => mmmidToContentID(id, pid));
    let contents = [];
    if (node.type === "mmmpen") {
      for (let i = 0; i < node.data.contents.length; i++) {
        contents.push(mmmidToContentID(node.data.contents[i], id));
      }

    }
    else { contents = []; }
    setNodes((nodes) => nodes.map((p) => {
      if (parentIDs.includes(p.id)) {
        let index = p.data.contents.indexOf(id);
        p.data.contents.splice(index, 1);
      }
      return p;
    })
    );
    // Delete all adjacent edges
    const connectedEdges = getConnectedEdges([getNode(id)], getEdges());
    const connectedEdgeIDs = connectedEdges.map((e) => (e.id));
    const edgesToDelete = connectedEdgeIDs.concat(aschild).concat(contents);
    const edgenodesToDelete = edgesToDelete.map((i) => edgeIDtoEdgenodeID(i));
    const nodesToDelete = edgenodesToDelete.concat(aschild).concat(contents);
    // If the landmark is an edgenode, delete the corresponding edge
    if (node.type === "mmmedgenode") {
      edgesToDelete.push(edgenodeIDtoEdgeID(id));
    }
    nodesToDelete.push(id);
    setEdges((edges) => edges.filter((edge) => !edgesToDelete.includes(edge.id)));
    setNodes((nodes) => nodes.filter((node) => !nodesToDelete.includes(node.id)));
  }, [id, getNode, getEdges, setNodes, setEdges, getNodes]);


  // HIDE 
  const hideLandmark = useCallback(() => {
    setNodes((nds) =>
      nds.map((node) => {
        if (node.id === id) {
          node.hidden = true;
        }
        return node;
      })
    );

    const node = getNode(id);
    if (node.data.kind.includes("edge")) {
      setEdges((egs) =>
        egs.map((e) => {
          if (e.id === id) {
            e.hidden = true;
          }
          return e;
        })
      );
    }
  }, [id, getNode, setNodes, setEdges]);

  // CHANGE
  const [form, setForm, onCloseForm, menu, setMenu, formDisabled, setFormDisabled, onCloseFormDisabled] = useContext(WidgetsContext);

  const editLandmark = useCallback(
    () => {
      setForm({
        id: id,
        data: landmark,
        isEditable: true
      });
    },
    [setForm, landmark]
  );

  const showInfo = useCallback(
    () => {
      setFormDisabled({
        id: id,
        data: landmark,
        isEditable: false
      });
    },
    [setFormDisabled, landmark]
  );

  const options = [
    { key: 1, className: "mmm-pen", onClick: showInfo, value: "info", icon: faCircleInfo, label: "Info about" },
    { key: 3, className: "mmm-question", onClick: editLandmark, value: "change", icon: faPen, label: "Edit" },
    { key: 2, className: "mmm-existence", onClick: duplicateLandmark, value: "duplicate", icon: faCopy, label: "Duplicate" },
    { key: 6, className: "mmm-narrative", onClick: hideLandmark, value: "hide", icon: faEyeSlash, label: "Hide" },
    { key: 4, className: "mmm-pit", onClick: deleteLandmark, value: "delete", icon: faTrash, label: "Delete" },
  ];

  return (
    <div
      style={{
        width: "300px",
        position: "absolute",
        border: "solid 1px #CCC",
        // borderRadius: 3,
        // backgroundColor: "#CCC",
        padding: 5,
        display: "flex",
        flexDirection: "column",
        top,
        left,
        zIndex: 1000,
        // background: "red",
      }}
      className="selectWrapper  context-menu"
      {...props}
    >
      <p style={{ padding: 5, margin: "0em", backgroundColor: "white" }} className="instruction">
        <small>⟟  Landmark: {id}</small>
      </p>
      {options.map((item) => (
        <div key={item.key}>
          <button  {...item}  ><span style={{ width: "6em", marginRight: "0.5em" }}> <FontAwesomeIcon icon={item.icon} /> </span> {item.label} this {props.data.kind}  </button>
        </div>
      ))}
    </div>
  );
}
