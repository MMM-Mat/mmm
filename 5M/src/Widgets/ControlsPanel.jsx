import React from 'react';
import ReactFlow, { Panel } from 'reactflow';
// Widgets
import DragNodes from './Subwidgets/DragNodes';
// Utils
import LayoutFlow from '../Utils/Layouting';
import SaveAsMMMButton from './Buttons/SaveAsMMM';
import { Upload } from './Buttons/Upload';
// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEyeSlash, faLink } from '@fortawesome/free-solid-svg-icons'


const ControlsPanel = () => {

  let style = { width: "150px", backgroundColor: "#efefefdf", padding: "15px", borderRadius: "3px", margin: "0" };

  return (
    <Panel position="top-right" style={style}>
      <LayoutFlow />
      <DragNodes />
      <Upload />
      <SaveAsMMMButton />

        <a style={{ fontSize: "10px", float: "right", marginTop: "10px" }} href="https://www.incaseofpeace.org/documents/planMMMproposal.pdf#page=84">2019 draft proposal</a>
    </Panel>
  );
};



export default ControlsPanel;