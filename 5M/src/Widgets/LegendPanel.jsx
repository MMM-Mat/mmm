import React from 'react';
import { Panel } from 'reactflow';

const LegendPanel = () => {
    return (
        <Panel
            position="top-left"
            style={{
                width: "100px",
                backgroundColor: "#efefefdf",
                padding: "15px",
                borderRadius: "3px",
                margin: "0"
            }}>
            <div
                style={{
                    textAlign: "center",
                    marginBottom: "5px"
                }}>
                Legend:
            </div>
            <div className="mmm-narrative MMMlegend  " >
                <div className="protommmnode-type-label" style={{ fontSize: "small" }}>Narrative </div>
                <span className="tooltiptext" >
                    Default node for statements.
                </span>
            </div>
            <div className="mmm-existence MMMlegend">
                <div className="protommmnode-type-label " style={{ fontSize: "small" }}>Existence </div>
                <span className="tooltiptext" >
                    Term or property
                </span>
            </div>
            <div className="mmm-question MMMlegend" >
                <div className="protommmnode-type-label" style={{ fontSize: "small" }}>Question </div>
                <span className="tooltiptext" >
                    Question
                </span>
            </div>
            <div className="mmm-action MMMlegend" >
                <div className="protommmnode-type-label" style={{ fontSize: "small" }}>Action </div>
                <span className="tooltiptext" >
                    Instructions, Todo notes
                </span>
            </div>
            <div className="mmm-data MMMlegend" >
                <div className="protommmnode-type-label" style={{ fontSize: "small" }}>Data </div>
                <div className="tooltiptext" >
                    values: Booleans, dates, integers
                </div>
            </div>
            <div className="mmm-pen MMMlegend" style={{ marginTop: "10px" }} >
                <div className="protommmnode-type-label" style={{ fontSize: "small" }}>Pen </div>
                <span className="tooltiptext" >
                    Enclosure
                </span>
            </div>
        </Panel>
    );
};

export default LegendPanel;