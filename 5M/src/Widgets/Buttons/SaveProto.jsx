import React, { useCallback } from 'react';
import { useReactFlow } from "reactflow";
// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSquareCheck, faSquareXmark } from '@fortawesome/free-solid-svg-icons'


const ProtoSaveButton = ({ id, className, kind }) => {


    const { getNode, setNodes, addNodes } = useReactFlow();

    const protonode = getNode(id);
    console.log("protonode is",protonode);

    // DELETE
    const deleteProtoNode = useCallback(() => {
        setNodes((nodes) => nodes.filter((node) => node.id !== id));
    }, [id, setNodes]);

    // CREATE NODE / PEN
    const createFinalNode = useCallback((type) => {
        const protonode = getNode(id);
        deleteProtoNode();
        const newID = id;
        const newNode = {
            type: type,
            id: newID,
            position: protonode.position,
            data: { ...protonode.data, mmmid: newID },
        };
        addNodes(newNode);
    }, [id, getNode, addNodes]);

    // SAVE
    const SaveProtoNode = useCallback(() => {
        const node = getNode(id);
        switch (kind) {
            case 'node':
                if (node.data.label === "") {
                    alert(`...Label of this ${node.data.type} ${node.data.kind} can't be empty!`);
                    return;
                } else {
                    createFinalNode('mmmnode');
                    return;
                }
            case 'pen':
                createFinalNode('mmmpen');
                return;
            default:
                console.log("Shouldn't be here", kind);
                return;
        };
    }, [id, getNode]);

    return (
        <div
            className={className}
            style={{
                float: "right",
                backgroundColor: "transparent"
            }}>

            <button
                title="Save landmark"
                className="controlbutton save" onClick={SaveProtoNode} value="save"><FontAwesomeIcon icon={faSquareCheck} /></button>
        </div>
    );

};

export default ProtoSaveButton;