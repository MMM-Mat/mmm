import React  from 'react';
import LandmarkInfo from '../Subwidgets/LandmarkInfo';
// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleInfo } from '@fortawesome/free-solid-svg-icons'

const LandmarkInfoControl = ({id, className, style}) => {
    
    return (
            <div className={"tooltip info "+ className} style={style}><FontAwesomeIcon icon={faCircleInfo} />
                <span className="tooltiptext" >
                    <LandmarkInfo id={id} />
                </span>
            </div>
    );
};

export default LandmarkInfoControl;
