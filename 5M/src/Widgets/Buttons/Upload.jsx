import React, { useState, useEffect } from "react";
import {
  useReactFlow,
} from 'reactflow';
import { MarkerType } from "reactflow";
// Utils
import { edgeMMMidToRFedgeID } from "../../Utils/SomeFunctions";

export function Upload() {
  const [files, setFiles] = useState("");
  const { addNodes, addEdges, getNodes, setNodes } = useReactFlow();

  const handleChange = e => {
    const fileReader = new FileReader();
    fileReader.readAsText(e.target.files[0], "UTF-8");

    fileReader.onload = e => {
      let mmmterr = JSON.parse(e.target.result.split('"edgesource":').join('"source":').split('"edgetarget":').join('"target":'));
      setFiles(mmmterr);
    };
  };

  useEffect(() => {
    console.log("useeffect upload");
    for (var i = 0; i < files.length; i++) {
      var obj = files[i];
      if (obj.kind !== "pit") {

        const newLandmark = {
          id: obj.mmmid,
          data: obj,
        };

        if (obj.kind.includes('edge')) {
          newLandmark.type = 'mmmedge';
          newLandmark.id = edgeMMMidToRFedgeID(newLandmark.id);

          if (obj.kind === "unidirectional edge") {
            newLandmark.sourceHandle = 'top';
            newLandmark.targetHandle = 'bottom';
          } else {
            newLandmark.sourceHandle = 'right';
            newLandmark.targetHandle = 'left';
          }
          newLandmark.source = obj.source;
          newLandmark.target = obj.target;
          newLandmark.markerEnd = {
            type: MarkerType.ArrowClosed,
            width: 7,
            height: 7,
            color: "#FF0072",
          };
          addEdges(newLandmark);
        }
        else if (obj.kind === "node") {
          newLandmark.position = {
            x: 0,
            y: 0
          };
          newLandmark.selected = false;
          newLandmark.origin = [0.5, 0.5];
          if (obj.hasOwnProperty("tags")) {
            let tag = obj.tags.find(item => item.includes('local file'));
            if (tag && obj.hasOwnProperty("marks")) {
              let mark = obj.marks.find((m) => typeof (m) === "object" && m.hasOwnProperty("local_file_path"));
              console.log(tag, obj.marks[0], typeof (obj.marks[0]), mark, "ok", mark.local_file_path);
              newLandmark.type = 'mmmfile';
              addNodes(newLandmark);
            }
            else {
              tag = obj.tags.find(item => item.includes('image'));
              if (tag) {
                newLandmark.type = 'mmmimage';
                addNodes(newLandmark);
              }
              else {
                newLandmark.type = 'mmmnode';
                addNodes(newLandmark);
              }
            }
          } else {
            newLandmark.type = 'mmmnode';
            addNodes(newLandmark);
          }
        }
        else if (obj.kind === "pen") {
          newLandmark.type = 'mmmpen';
          newLandmark.position = {
            x: 0,
            y: 0
          };
          newLandmark.style = { zIndex: "-1" }
          newLandmark.selected = false;
          addNodes(newLandmark);
        }
      }
    }

  }, [files, setNodes, getNodes, addEdges, addNodes]);

  return (
    <form style={{ marginTop: "20px" }}>
      <input id="fileUpload"
        className={"inputfile"}
        type="file"
        onChange={handleChange}
        accept="text/json"
        title={"Upload a mmm.json file"}
      />
      <label style={{ paddingLeft: "0px", paddingRight: "0px" }} htmlFor="fileUpload"
        title="Upload a mmm.json file" className="mybtn full">Upload mmm.json  </label>
    </form>
  );
}

