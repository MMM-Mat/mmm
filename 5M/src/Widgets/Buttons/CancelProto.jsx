import React, { useCallback } from 'react';
import { useReactFlow } from "reactflow";
// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSquareCheck, faSquareXmark } from '@fortawesome/free-solid-svg-icons'

const ProtoCancelButton = ({ className, id }) => {

    const { setNodes } = useReactFlow();

    // DELETE
    const deleteProtoNode = useCallback(() => {
        setNodes((nodes) => nodes.filter((node) => node.id !== id));
    }, [id, setNodes]);

    return (
        <div className={className}
            style={{
                float: "right",
                backgroundColor: "transparent"
            }}
        >
            <button
                title="Delete/Cancel"
                className={className + " controlbutton cancel"}
                onClick={deleteProtoNode} value="delete"><FontAwesomeIcon icon={faSquareXmark} style={{ marginTop: "-20px" }} /></button>

        </div>
    );

};

export default ProtoCancelButton;