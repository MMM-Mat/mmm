import React, { useCallback } from 'react';
import { useReactFlow } from "reactflow";

const MMMKeys = ['mmmid', 'label', 'kind', 'type', 'status', 'edgesource', 'edgetarget', 'authorships', 'tags', 'marks', 'timestamp', 'contents', 'source', 'target'];

const SaveAsMMMButton = () => {

    const { getNodes } = useReactFlow();

    const saveAsMMMJSON = useCallback(() => {
        // Prepare JSON blob
        let raw_mmmterritory = [];
        let n = getNodes();
        for (var i = 0; i < n.length; ++i) { raw_mmmterritory.push(n[i].data); }

        const mmmterritory = raw_mmmterritory.map(function (raw_landmark) {
            var filtered_landmark = Object.fromEntries(
                Object.entries(raw_landmark).filter(
                   ([key, val])=>MMMKeys.includes(key)
                )
             );
            return filtered_landmark;
        });
        let jsonterrA = JSON.stringify(mmmterritory, null, 2);
        let jsonterr = jsonterrA.split('"source":').join('"edgesource":').split('"target":').join('"edgetarget":').split('"id":').join('"mmmid":');
        const blob = new Blob([jsonterr], { type: 'application/json' });
        const data = URL.createObjectURL(blob);
        const filename = 'mynotes.mmm.json';
        const aTag = document.createElement('a');
        aTag.download = filename;
        aTag.href = data;

        aTag.addEventListener('click', (e) => {
            setTimeout(() => URL.revokeObjectURL(aTag.href), 30 * 1000);
        });
        aTag.click();

    }, [getNodes]);

    return (
        <button
            title="Save data in MMM file" onClick={() => saveAsMMMJSON()} className="mybtn full" value="Save data">Save data</button>
    );

};

export default SaveAsMMMButton;