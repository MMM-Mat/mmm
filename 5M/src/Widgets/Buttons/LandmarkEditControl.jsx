import React, { useContext, useCallback } from 'react';
// Contexts
import { WidgetsContext } from "../../Contexts/WidgetsContextProvider";
// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPen } from '@fortawesome/free-solid-svg-icons'

const LandmarkEditControl = ({ id, className }) => {

    const [_, setForm] = useContext(WidgetsContext);

    const onNodeOpenForm = useCallback(
        () => {
            setForm({
                id: id,
                isEditable: true
            });
        },
        [setForm, id]
    );

    return (
        <button title="Edit landmark" className={className + " controlbutton edit"} onClick={onNodeOpenForm} value="edit"><FontAwesomeIcon icon={faPen} /></button>

    );
};

export default LandmarkEditControl;
