import React, { useCallback } from 'react';
import { useReactFlow } from "reactflow";
// Widgets
import EditableText from './Subwidgets/EditableText.jsx';

export default function LandmarkLabel({ style, className, placeholder, id }) {

    const { getNode, setNodes } = useReactFlow();
    const node = getNode(id);

    const handleLabelChange = useCallback(
        (newtext) => {
            setNodes((nds) =>
                nds.map((node) => {
                    if (node.id === id) {
                        node.data.label = newtext;
                    }
                    return node;
                })
            );
            return 0;
        },
        [id, setNodes]
    );

    return (
        <EditableText
            style={style}
            className={className}
            onTextChange={handleLabelChange}
            initialText={node.data.label}
            placeholder={placeholder}
            proto={node.type.includes("proto")} />

    );

}

