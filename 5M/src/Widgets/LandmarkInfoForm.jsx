import React, { useState, useContext, useCallback } from 'react';
import { useReactFlow } from "reactflow";
// WIDGETS
import KindSelector from './Subwidgets/KindSelector.jsx';
import TypeSelector from './Subwidgets/TypeSelector.jsx';
import LabelInputer from './FormWidgets/LabelInputer.jsx';
import TagsInputer from './FormWidgets/TagsInputer.jsx';
import MarksInputer from './FormWidgets/MarksInputer.jsx';
import AuthorshipsInputer from './FormWidgets/AuthorshipsInputer.jsx';
import StatusUpdater from './FormWidgets/StatusUpdater.jsx';
// Context:
import { LandmarkContext } from '../Contexts/LandmarkContextProvider.js'
import { WidgetsContext } from '../Contexts/WidgetsContextProvider';
import { TemporaryContext } from '../Contexts/TemporaryContext';
// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleInfo, faMapPin, } from '@fortawesome/free-solid-svg-icons'
// Utils
import { simplifyKind } from "../Utils/MMMfields.js";

function getCSSClass(c, k, t) {
  if (k = "pen") {
    return [c + " mmm-pen", c + " type-label  mmm-pen"];
  }
  else if (k === "node") {
    return [c, c + " type-label  mmm-" + t];
  }
  else if (k.includes("edge")) {
    return [c + " mmm-edge", c + " type-label  mmm-" + t + "-edge"];
  }
  else {
    console.log("ERROR");
  }
}

const LandmarkInfoForm = () => {

  const [form, setForm, onCloseForm] = useContext(WidgetsContext);
  const { getNode, setNodes } = useReactFlow();
  const id = form.id;
  const node = getNode(id);
  const [landmark, setLandmark] = useState(node.data);
  const [copy, setCopy] = useState(JSON.parse(JSON.stringify(landmark)));
  const [kindclass, typeclass] = getCSSClass("mmmselector", simplifyKind(copy.kind), copy.type);

  const onSave = useCallback(
    () => {
      const node = getNode(id);
      if (copy.kind === "pen" && copy.hasOwnProperty('label')) {
        delete copy.label;
      }
      setNodes((nds) =>
        nds.map((node) => {
          if (node.id === id) {
            node.data = JSON.parse(JSON.stringify(copy));
          }
          return node;
        })
      );
      setForm(null);
    },
    [id, setForm, copy, getNode, setNodes]
  );

  return (
    <TemporaryContext.Provider value={[copy, setCopy]}>
      <LandmarkContext.Provider value={[landmark, setLandmark]}>
        <div className="overlay " >
          <div className="form-container" >
            <div className="form-containerBis" >
              <h1 className="text-center">Landmark Information</h1>

              <div style={{ display: "flex", justifyContent: "space-between", marginBottom: "10px" }}>
                {/* ******************************************** 
                  **       MMMID + TIMESTAMP + STATUS       **
                  ******************************************** */}
                <div className="left-form-col">
                  <div style={{ color: "#CCC", fontWeight: "200", marginTop: "10px" }}>
                    <span style={{ textTransform: "uppercase" }}>mmm id:</span>
                    <span style={{ margin: "0 5px" }} >{copy.mmmid} </span> <FontAwesomeIcon icon={faMapPin} />
                  </div>
                  <div style={{ color: "#CCC", fontWeight: "200", marginTop: "10px" }} >
                    <span style={{ textTransform: "uppercase" }}>
                      Timestamp
                      <div className="tooltip" style={{ margin: "0 5px" }} >
                        <FontAwesomeIcon icon={faCircleInfo} />
                        <span style={{ textTransform: "none" }} className="tooltiptext">The timestamp corresponds to the date at which the contribution was recorded on your machine, possibly later than when the landmark was created by its authors.</span>
                      </div>:
                    </span>
                    <span style={{ margin: "0 5px" }} >{copy.timestamp}</span>
                  </div>
                  <div style={{ display: "flex", justifyContent: "space-between", marginTop: "10px" }}>
                    <span style={{ textTransform: "uppercase" }} className="label">
                      Status <div className="tooltip" style={{ margin: "0 5px" }} >
                        <FontAwesomeIcon icon={faCircleInfo} />
                        <span style={{ textTransform: "none" }} className="tooltiptext">Status, by default is private. If the status is anything else than private, it is deliberately so.</span>
                      </div>:
                    </span>
                    <div style={{ marginLeft: "30px", flex: "1" }} >
                      <StatusUpdater />
                    </div>

                  </div>
                </div>
                {/* ******************************************** 
                  **                AUTHORSHIPS               **
                  ******************************************** */}
                <div className="right-form-col responsive " >
                  <AuthorshipsInputer />
                </div>
              </div>

              {/* ******************************************** 
                   **            FORM                        **
                   ******************************************** */}
              <div>
                {/* ******************************************** 
                     **         ROW 1 : Kind + Type            **
                     ******************************************** */}
                <div className="row">
                  {/* Left Column : Kind */}
                  <div className="left-form-col" >
                    <div className="label">Kind </div>
                    <KindSelector className={kindclass} isDisabled={true} />
                  </div>
                  {/* Right Column : Type */}
                  <div className="right-form-col responsive " >
                    <div className="label">Type</div>
                    <TypeSelector className={typeclass} />
                  </div>
                </div>
                {/* ******************************************** 
                    **         ROW 2 : LABEL                **
                    ******************************************** */}
                {(copy.kind !== "pen") &&
                  <div className="responsive lineContainer">
                    <LabelInputer />
                  </div>
                }

                {/* ******************************************** 
                    **         ROW 3 : TAGS & MARKS           **
                    ******************************************** */}
                <div className="row">
                  {/* Left Column */}
                  <div className="left-form-col" style={{ display: "flex" }} >
                    <TagsInputer />
                  </div>
                  {/* Right Column */}
                  <div className="right-form-col responsive" style={{ display: "flex" }}>
                    <MarksInputer />
                  </div>
                </div>

                {/* ******************************************** 
                    **         Last ROW  : Buttons            **
                    ******************************************** */}
                <div className="row">
                  {/* Left Column : Close  */}
                  <div className="left-form-col ">
                    <button className="form-button mmmselector" onClick={onCloseForm} >Close/Cancel</button>
                  </div>
                  {/* Right Column : Save */}
                  <div className="right-form-col responsive">
                    <button className="form-button mmmselector" title="Save Change(s)" onClick={onSave}>Save Change(s)</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </LandmarkContext.Provider >
    </TemporaryContext.Provider >
  );
};

export default LandmarkInfoForm;
