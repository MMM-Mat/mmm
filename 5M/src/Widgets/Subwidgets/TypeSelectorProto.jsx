import React from 'react';

const TypeSelector = ({ className, mmmtype, mmmkind, style, onChange }) => {

  let options;
  if (mmmkind === "node") {
    options = [
      { key: 0, value: 'instruction', className: "instruction", label: "Choose type", disabled: true },
      { key: 1, value: 'narrative', label: 'Narrative', className: "mmm-narrative" },
      { key: 2, value: 'existence', label: 'Existence', className: "mmm-existence" },
      { key: 3, value: 'question', label: 'Question', className: "mmm-question" },
      { key: 4, value: 'action', label: 'Todo', className: "mmm-action" },
      { key: 5, value: 'data', label: 'Data Value', className: "mmm-data" },
    ];
  } else if (mmmkind === "pen") {
    options = [
      { key: 10, value: 'instruction', className: "instruction", label: "Choose type", disabled: true },
      { key: 11, value: 'default', label: 'default', className: "mmm-pen" },
      { key: 12, value: 'definition', label: 'definition', className: "mmm-pen" },
      { key: 13, value: 'folder', label: 'Folder', className: "mmm-pen" },
      { key: 14, value: 'glossary', label: 'Glossary', className: "mmm-pen" },
      { key: 15, value: 'reasons', label: 'Reasons', className: "mmm-pen" },
      { key: 16, value: 'data', label: 'Measure', className: "mmm-data" },
    ];
  } else if (mmmkind.includes("edge")) {
    options = [
      { key: 20, value: 'instruction', className: "instruction", label: "Choose type", disabled: true },
      { key: 21, value: 'relate', label: 'Relate (default adirectional)', className: "mmm-edge" },
      { key: 212, value: 'relatesto', label: 'Relates to (default unidirectional)', className: "mmm-edge" },
      { key: 22, value: 'equates', label: 'Equates', className: "mmm-edge" },
      { key: 23, value: 'differsfrom', label: 'Differs From', className: "mmm-edge" },
      { key: 24, value: 'pertains', label: 'Pertains', className: "mmm-edge" },
      { key: 25, value: 'answers', label: 'Answers', className: "mmm-edge" },
      { key: 26, value: 'questions', label: 'Questions', className: "mmm-edge" },
      { key: 27, value: 'instantiates', label: 'Instantiates', className: "mmm-edge" },
      { key: 28, value: 'substantiates', label: 'Substantiates', className: "mmm-edge" },
      { key: 29, value: 'nuances', label: 'Nuances', className: "mmm-edge" },
      { key: 30, value: 'supports', label: 'Supports', className: "mmm-edge" },
      { key: 31, value: 'precedes', label: 'Precedes', className: "mmm-edge" },
      { key: 32, value: 'pennedin', label: 'Penned in', className: "mmm-edge" },
    ];
  }

  return (
    <div >
      <select
        onChange={(event) => { onChange(event.target.value); }}
        className={className}
        style={style}
        value={mmmtype}>
        {options.map((item) => (
          <option {...item} />
        ))}
      </select>
    </div >
  );
};

export default TypeSelector;