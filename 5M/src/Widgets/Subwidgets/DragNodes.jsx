import React from 'react';
import DragNode from '../../Landmarks/DragNode.jsx';

export default () => {
    const onDragStart = (event, nodeType) => {
        event.dataTransfer.setData('application/reactflow', nodeType);
        event.dataTransfer.effectAllowed = 'move';
    };

    const createEmptyNode = (event, nodeType) => {
    };

    return (
        <div>
            <div className="description" style={{ marginTop: "20px", width: "150px" }}>Drag these nodes onto the workspace to create new ones:</div>

            <div className="dndnode" onDragStart={(event) => onDragStart(event, 'node protonode narrative')} draggable onClick={createEmptyNode}>
                <DragNode mmmtype="narrative" />
            </div>
            <div className="dndnode" onDragStart={(event) => onDragStart(event, 'node protonode question')} draggable>
                <DragNode mmmtype="question" />
            </div>
            <div className="dndnode" onDragStart={(event) => onDragStart(event, 'node protonode existence')} draggable>
                <DragNode mmmtype="existence" />
            </div>

            <div className="dndnode" onDragStart={(event) => onDragStart(event, 'pen protopen default')} draggable>
                <DragNode mmmtype="pen" />
            </div>
        </div>

    );
};