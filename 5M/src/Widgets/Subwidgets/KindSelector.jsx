import React, { useContext } from 'react';
// Contexts
import { LandmarkContext } from '../../Contexts/LandmarkContextProvider';
import { TemporaryContext } from "../../Contexts/TemporaryContext";
// Functions
import { getDefaultType, simplifyKind, adjustEdgekind } from "../../Utils/MMMfields";

const KindSelector = ({ className, isDisabled }) => {

  const [copy, setCopy] = useContext(TemporaryContext);
  const [landmark, _] = useContext(LandmarkContext)

  const onChange = (event) => {
    const newtype = getDefaultType(event.target.value, landmark.kind, landmark.type);
    setCopy({ ...copy, kind: adjustEdgekind(event.target.value, newtype), type: newtype });
  }

  const options = [
    { key: 1, value: 'instruction', className: "instruction", disabled: true, hidden: true },
    { key: 2, value: 'node', label: 'Node', className: "mmm-undefined" },
    { key: 3, value: 'edge', label: 'Edge', className: "mmm-edge" },
    { key: 4, value: 'pen', label: 'Pen', className: "mmm-pen" },
    { key: 5, value: 'pit', label: 'Pit', className: "mmm-pit", disabled: true },
  ];

  return (
    <div >
      <select onChange={onChange} className={className} defaultValue={simplifyKind(copy.kind)} disabled>
        {options.map((item) => (
          <option {...item} />
        ))}
      </select>
    </div >
  );
};

export default KindSelector;