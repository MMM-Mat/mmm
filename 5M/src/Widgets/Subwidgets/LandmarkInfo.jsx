import React from 'react';
import { useReactFlow } from "reactflow";
// Utils
import { printArrayAsString } from '../../Utils/SomeFunctions';
import { getParents } from '../../Utils/algo';

const LandmarkInfo = ({ id }) => {

  const { getNode, getNodes } = useReactFlow();
  const node = getNode(id);
  const landmark = node.data;
  const parents = getParents(landmark.mmmid, getNodes()).map((p) => p.id);

  return (
    <div style={{ fillOpacity: "50%" }}>
      <div>Landmark ID: {landmark.mmmid}</div>
      <div> Kind: {landmark.kind}</div>
      <div> Type: {landmark.type}</div>

      {landmark.label && <div> Label: "{landmark.label}"</div>}

      {landmark.kind === "pen" && landmark.contents && <div> Contents: {printArrayAsString(landmark.contents)}</div>}
      {landmark.tags && <div> Tags: {printArrayAsString(landmark.tags)} </div>}
      {landmark.marks && <div> Marks: {printArrayAsString(landmark.marks)} </div>}
      {landmark.source && landmark.kind.includes("edge") && <div> Source: {landmark.source}</div>}
      {landmark.target && landmark.kind.includes("edge") && <div> Target: {landmark.target}</div>}
      {parents.length > 0 && <div>Parent Pens:
        <ul style={{ marginTop: "0px" }}>
          {parents.map((item, index) => (<li key={index}>{item}</li>))}
        </ul></div>}

      {landmark.status && <div>Status: {landmark.status}</div>}
      {landmark.timestamp && <div> Timestamp: {landmark.timestamp} </div>}
      {landmark.authorships && landmark.authorships.length > 0 && <div> Authorships:
        {(landmark.authorships).map((item, index) => (
          <div style={{ marginLeft: "10px" }} key={item.timestamp + landmark.mmmid + index}>
            <span style={{ marginRight: "1px" }}>{item.timestamp} --</span>

            <span style={{ margin: "0 5px" }} >{printArrayAsString(item.team)}
            </span>
          </div>
        ))}
      </div>}
    </div>
  );
};

export default LandmarkInfo;
