import React from 'react';
import { useOnSelectionChange } from "reactflow";

const LandmarkSelector = ({ different, setSelected }) => {

    useOnSelectionChange({
        onChange: ({ nodes, edges }) => {
            console.log("changed selection:", nodes, setSelected);

            setSelected(nodes.filter((node) => { if (node.id !== different) { return node } }).map((node) => node.id));
        },
    });

    return (
        <div>
        </div>
    );

};

export default LandmarkSelector;