import React, { useState } from 'react';

const EditableText = ({ onTextChange, className, initialText, placeholder, proto, style }) => {

  const [isEditing, setIsEditing] = useState(proto);
  const [text, setText] = useState(initialText);

  const handleDoubleClick = () => {
    setText(initialText);
    setIsEditing(true);
  };

  const handleBlur = (event) => {
    if (text !== "") {
      onTextChange(text);
      if (!proto) {
        setIsEditing(false);
      }
    }
  };

  return (
    <div onDoubleClick={handleDoubleClick} className={className} >

      {isEditing ? (
        <textarea
          style={style}
          rows={3}
          // cols={}
          type="text"
          value={text}
          onChange={(e) => setText(e.target.value)}
          onBlur={handleBlur}
          // onMouseLeave={()=> {setIsEditing(false); onTextChange(text);}}
          placeholder={placeholder}
        />
      ) : (
        <span >{initialText}</span>
      )}
    </div>
  );

};

export default EditableText;

// https://www.smashingmagazine.com/2021/03/outside-focus-click-handler-react-component/

// https://medium.com/@zahidbashirkhan/implementing-double-click-to-edit-text-in-react-2e1d4bcb2493

// https://codesandbox.io/p/sandbox/react-dev-64n8l5?file=%2Fsrc%2FApp.js%3A20%2C14&utm_medium=sandpack

// https://react.dev/learn/extracting-state-logic-into-a-reducer