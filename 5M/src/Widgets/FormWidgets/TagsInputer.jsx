import React, { useState, useContext } from 'react';
import { TemporaryContext } from "../../Contexts/TemporaryContext";
// Subwidgets
import { InputAndButtons } from './InputAndButtons.jsx';
// Utils
import { printArrayAsString } from '../../Utils/SomeFunctions';
// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleInfo, faPlus, faArrowRight } from '@fortawesome/free-solid-svg-icons'

const TagsInputer = () => {
    const [copy, setCopy] = useContext(TemporaryContext);
    const [inputtagfield, setInputtagfields] = useState(false);

    const updateTags = (event) => {
        // Prevent the browser from reloading the page
        event.preventDefault();
        const command = event.nativeEvent.submitter.attributes.type.nodeValue;
        if (command === "cancel") {
            setInputtagfields(false);
        }
        else if (command === "submit") {
            const form = event.target;
            const formData = new FormData(form);
            const formJson = Object.fromEntries(formData.entries());
            const newtag = formJson.myInput;

            if (newtag === "") {
                alert(`The tag can't be an empty string.`);
            }
            else if (copy.tags.includes(newtag)) {
                alert(`The tag ${newtag} was already there. I'm not adding it again.`);
                setInputtagfields(false);
            }
            else {
                setCopy({ ...copy, tags: [...copy.tags, newtag] });
                setInputtagfields(false);
            }
        }
    }



    return (

        <div style={{
            border: "#91949d 1px solid", padding: "20px 20px", borderRadius: "2px", width: "100%",
        }}>
            <span className="label">
                Tags
                <div className="tooltip" style={{ margin: "0 5px" }} >
                    <FontAwesomeIcon icon={faCircleInfo} />
                    <span style={{ textTransform: "none" }} className="tooltiptext">Tags are like private tags, especially useful for personal house-keeping.</span>
                </div>:
            </span>
            <div style={{ marginLeft: "0px", marginTop: "5px" }} >
                <div>

                    {copy.hasOwnProperty("tags") &&
                        <div>
                            {printArrayAsString(copy.tags)}
                        </div>}


                    {/* {(copy.tags).map((item) => (<span key={item}> {item},</span>))} */}


                    {/* {inputtagfield &&
                        <form className={"flex-input-field-container"} onSubmit={updateTags}>
                            {newtag}
                            <input name="myInput" className="flex-input-field" key="newtag" />
                            <button type="submit" className="form-input-save-button"><FontAwesomeIcon icon={faArrowRight} /></button>
                        </form>
                    } */}

                    {inputtagfield &&
                        <form onSubmit={updateTags}>
                            <InputAndButtons placeholder="Your new tag" />
                        </form>
                    }

                    {!inputtagfield && <div >
                        <button
                            style={{ margin: "20px 10px 10px 0" }}
                            onClick={() => {
                                if (!copy.hasOwnProperty('tags')) {
                                    setCopy({ ...copy, tags: [] });
                                }
                                setInputtagfields(true);
                            }}
                            type="button"
                            className="IB-btn "
                        >
                            <FontAwesomeIcon icon={faPlus} />
                        </button>
                        <span className="sublabel"  >Add a tag</span>
                    </div>}

                </div>
            </div>
        </div>
    );
}

export default TagsInputer;  