import React, { useContext } from 'react';
import { TemporaryContext } from "../../Contexts/TemporaryContext";

const StatusUpdater = () => {

    const [copy, setCopy] = useContext(TemporaryContext);

    const handleClick = (event) => {
        setCopy({ ...copy, status: event.target.value });
    }
    const options = [
        { key: 1, value: "private", label: "Private", onChange: handleClick, checked: (copy.status === "private") },
        { key: 2, value: "public", label: "Public", onChange: handleClick, checked: (copy.status === "public") },
        { key: 3, value: "shared", label: "Shared with...", onChange: handleClick, checked: (copy.status === "shared") },
    ];

    return (
        <div>
            {options.map((item) => (
                <div key={item.key}><input type="radio" className="subject-option" {...item} /> {item.label}</div>
            ))}
        </div>
    );
}

export default StatusUpdater;  