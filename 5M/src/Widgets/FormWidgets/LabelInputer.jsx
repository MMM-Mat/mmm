import React, {  useContext } from 'react';
import { TemporaryContext } from "../../Contexts/TemporaryContext";


const LabelInputer = () => {
  const [copy, setCopy] = useContext(TemporaryContext);

  const handleChange = (event) => {
    setCopy({ ...copy, label: event.target.value });
  };

  return (
    <div className="right">
      <div className="label">Label</div>
      <textarea className="doubleinput-field" rows="4" defaultValue={copy.label} onChange={handleChange} />
    </div>
  );
}

export default LabelInputer;  