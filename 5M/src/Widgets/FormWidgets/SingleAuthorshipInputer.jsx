import React, { useState} from 'react';
// Utils
import { getActualTimestamp, printArrayAsString } from "../../Utils/SomeFunctions";
// Subwidgets
import { InputAndButtons } from './InputAndButtons.jsx';
// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleInfo, faPlus, faPen, faTrash, faXmark, faCheck } from '@fortawesome/free-solid-svg-icons'

const SingleAuthorshipInputer = ({ onClose, onSave }) => {
    const [inputAuthorField, setInputAuthorField] = useState(true);
    const [newAuthorship, setNewAuthorship] = useState({ timestamp: getActualTimestamp(), team: [] });
    const [team, setTeam] = useState([]);

    const addAnAuthorInputField = () => {
        setInputAuthorField(true);
    }

    const addAuthor = (author) => {
        const authorObject = { name: author, isEditing: false };
        setTeam([...team, authorObject]);
        setNewAuthorship({ ...newAuthorship, team: [...newAuthorship.team, author] });
    }
    const deleteAuthor = (author) => {
        setTeam((team) => team.filter((a) => a.name !== author));
        setNewAuthorship({ ...newAuthorship, team: newAuthorship.team.filter(function (a) { return a !== author; }) });
    }

    const updateAuthor = (event) => {
        // Prevent the browser from reloading the page
        event.preventDefault();
        const command = event.nativeEvent.submitter.attributes.type.nodeValue;
        console.log("command", command);
        const form = event.target;
        const author = form[1].attributes.data.nodeValue;
        const authorObject = team.find((element) => element.name === author);
        const i = team.findIndex(x => x.name === author);
        console.log(team[i].name, author, "was editable:", authorObject.isEditing, team[i].isEditing, team);

        if (command === "cancel") {
            if (authorObject.isEditing) {
                // Toggle Editableness:
                const updatedTeam = team.map((a) => {
                    if (a.name === author) { return { ...a, isEditing: !team[i].isEditing }; }
                    else { return a; }
                });
                setTeam(updatedTeam);
            }
            else { deleteAuthor(author); }
        }
        else if (command === "submit") {
            if (team[i].isEditing) {
                // Get  the modified author name:
                const formData = new FormData(form);
                const formJson = Object.fromEntries(formData.entries());
                const newauthor = formJson.myInput;

                const updatedTeam = team.map((a) => {
                    if (a.name === author) { return { ...a, isEditing: !team[i].isEditing, name: newauthor }; }
                    else { return a; }
                });
                setTeam(updatedTeam);
                setNewAuthorship({ ...newAuthorship, team: newAuthorship.team.map(a => { if (a === author) { return newauthor } else { return a } }) });
            }
            else {
                const updatedTeam = team.map((a) => {
                    if (a.name === author) { return { ...a, isEditing: !team[i].isEditing }; }
                    else { return a; }
                });
                setTeam(updatedTeam);
            }
        }
    }

    const updateTeam = (event) => {
        // Prevent the browser from reloading the page
        event.preventDefault();
        const command = event.nativeEvent.submitter.attributes.type.nodeValue;
        console.log("command", command);

        if (command === "cancel") {
            setInputAuthorField(false);
        }
        else if (command === "submit") {
            const form = event.target;
            const formData = new FormData(form);
            const formJson = Object.fromEntries(formData.entries());
            const newauthor = formJson.myInput;
            if (newauthor === "") {
                alert(`The author can't be an empty string.`);
            }
            else if (newAuthorship.team.includes(newauthor)) {
                alert(`The author name ${newauthor} was already there. I'm not adding it again.`);
                setInputAuthorField(false);
            }
            else {
                addAuthor(newauthor);
                setInputAuthorField(false);
            }
        }
    }

    return (
        <div style={{
            marginTop: "10px", border: "#91949d 1px solid", padding: "10px 10px", borderRadius: "2px", backgroundColor: "#91949d", color: "white"
        }}>
            <div style={{ color: "#CCC", fontWeight: "200", margin: "5px 0" }} >
                <span style={{ textTransform: "uppercase" }}>Timestamp
                    <div className="tooltip" style={{ margin: "0 5px" }} >
                        <FontAwesomeIcon icon={faCircleInfo} />
                        <span style={{ textTransform: "none" }} className="tooltiptext">The timestamp corresponds to the date at which the contribution was recorded on your machine, possibly later than when the landauthor was created by its authors.</span>
                    </div>:
                </span>
                <span style={{ margin: "0 5px" }} >{newAuthorship.timestamp}</span>
            </div>

            <div style={{ color: "#CCC", fontWeight: "200", margin: "5px 0" }} >
                <span style={{ textTransform: "uppercase" }}>Team
                    <div className="tooltip" style={{ margin: "0 5px" }} >
                        <FontAwesomeIcon icon={faCircleInfo} />
                        <span style={{ textTransform: "none" }} className="tooltiptext">The team is the set of co-authors. Order is not significant.</span>
                    </div>:
                </span>
                <span style={{ margin: "0 5px" }} >{printArrayAsString(newAuthorship.team)}
                </span>
            </div>

            {(team).map((item) => (
                <form onSubmit={updateAuthor} key={item.name}>
                    <InputAndButtons
                        key={item.name}
                        placeholder={item.name}
                        data={item.name}
                        leftIcon={item.isEditing ? faXmark : faTrash}
                        rightIcon={item.isEditing ? faCheck : faPen}
                        disabled={!item.isEditing}
                    />
                </form>
            ))}

            {inputAuthorField &&
                <form onSubmit={updateTeam}>
                    <InputAndButtons placeholder="Author name" leftIcon={faTrash} />
                </form>
            }

            {!inputAuthorField && <div >
                <button style={{ margin: "4px 10px 4px 0" }} onClick={addAnAuthorInputField} type="button" className="IB-btn "><FontAwesomeIcon icon={faPlus} /></button>
                <span className="sublabel" style={{ color: "white" }} >Add an author</span>
            </div>}

            <div >
                <button style={{ margin: "4px 10px 4px 0" }} onClick={onClose} type="button" className="IB-btn "><FontAwesomeIcon icon={faXmark} /></button>
                <span className="sublabel" style={{ color: "white" }} >Cancel this authorship</span>
            </div>

            <div >
                <button style={{ margin: "4px 10px 5px 0" }} onClick={() => { onSave(newAuthorship) }} type="button" className="IB-btn "><FontAwesomeIcon icon={faCheck} /></button>
                <span className="sublabel" style={{ color: "white" }} >Save this authorship</span>
            </div>
        </div>
    );
}

export default SingleAuthorshipInputer;


