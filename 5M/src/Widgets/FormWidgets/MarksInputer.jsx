import React, { useState, useContext } from 'react';
import { TemporaryContext } from "../../Contexts/TemporaryContext";
// Subwidgets
import { InputAndButtons } from './InputAndButtons.jsx';
// Utils
import { printArrayAsString } from '../../Utils/SomeFunctions';
// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleInfo, faPlus, faArrowRight } from '@fortawesome/free-solid-svg-icons'


const MarksInputer = () => {
    const [copy, setCopy] = useContext(TemporaryContext);
    const [inputmarkfield, setInputmarkfields] = useState(false);

    const updateMarks = (event) => {
        // Prevent the browser from reloading the page
        event.preventDefault();
        const command = event.nativeEvent.submitter.attributes.type.nodeValue;
        if (command === "cancel") {
            setInputmarkfields(false);
        }
        else if (command === "submit") {
            const form = event.target;
            const formData = new FormData(form);
            const formJson = Object.fromEntries(formData.entries());
            const newmark = formJson.myInput;
            if (newmark === "") {
                alert(`The mark can't be an empty string.`);
            }
            else if (copy.marks.includes(newmark)) {
                alert(`The mark ${newmark} was already there. I'm not adding it again.`);
                setInputmarkfields(false);
            }
            else {
                setCopy({ ...copy, marks: [...copy.marks, newmark] });
                setInputmarkfields(false);
            }
        }
    }

    return (
        <div style={{
            border: "#91949d 1px solid", padding: "20px 20px", borderRadius: "2px", width: "100%",
        }}>
            <span className="label">
                Marks
                <div className="tooltip" style={{ margin: "0 5px" }} >
                    <FontAwesomeIcon icon={faCircleInfo} />
                    <span style={{ textTransform: "none" }} className="tooltiptext">Marks are like private tags, especially useful for personal house-keeping.</span>
                </div>:
            </span>
            <div style={{ marginLeft: "0px", marginTop: "5px" }} >
                <div>
                    {copy.hasOwnProperty("marks") &&
                        <div>
                            {printArrayAsString(copy.marks)}
                        </div>}

                    {inputmarkfield &&
                        <form onSubmit={updateMarks}>
                            <InputAndButtons placeholder="Your new mark" />
                        </form>
                    }

                    {!inputmarkfield && <div >
                        <button
                            style={{ margin: "20px 10px 10px 0" }}
                            onClick={() => {
                                if (!copy.hasOwnProperty('marks')) {
                                    setCopy({ ...copy, marks: [] });
                                }
                                setInputmarkfields(true);
                            }}
                            type="mybutton"
                            className="IB-btn "
                        >
                            <FontAwesomeIcon icon={faPlus} />
                        </button>
                        <span className="sublabel"  >Add a mark</span>
                    </div>}

                </div>
            </div>
        </div>
    );
}

export default MarksInputer;  