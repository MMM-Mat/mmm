import React, { useState, useContext } from 'react';
import { TemporaryContext } from "../../Contexts/TemporaryContext";
// Utils
import { printArrayAsString } from "../../Utils/SomeFunctions";
// Subwidgets
import SingleAuthorshipInputer from './SingleAuthorshipInputer';
// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleInfo, faPlus, faArrowRight } from '@fortawesome/free-solid-svg-icons'

const AuthorshipsInputer = () => {
    const [copy, setCopy] = useContext(TemporaryContext);
    const [inputAuthorshipField, setInputAuthorshipField] = useState(false);

    const updateAuthorships = (newAuthorship) => {
        setInputAuthorshipField(false);
        if (copy.hasOwnProperty('authorships')) {
            setCopy({ ...copy, authorships: [...copy.authorships, newAuthorship] });
        }
        else {
            setCopy({ ...copy, authorships: [newAuthorship] });
        }
    }

    return (
        <div style={{
            marginTop: "10px", minWidth: "250px",
            maxWidth: "340px"
        }}>
            <span className="label">
                Authorships
                <div className="tooltip" style={{ margin: "0 5px" }} >
                    <FontAwesomeIcon icon={faCircleInfo} />
                    <span style={{ textTransform: "none" }} className="tooltiptext">One authorship is comprised of a list of authors and a timestamp. One MMM landmark may have several authorships.</span>
                </div>:
            </span>

            <div style={{ marginLeft: "0px", marginTop: "5px" }} >
                <div>
                    {copy.hasOwnProperty("authorships") ?
                        <div>
                            {(copy.authorships).map((item) => (
                                <div key={item.timestamp}>
                                    <span style={{ marginRight: "1px" }}>{item.timestamp} --</span>

                                    <span style={{ margin: "0 5px" }} >{printArrayAsString(item.team)}
                                    </span>
                                </div>
                            ))}
                        </div> : <div>Unknown</div>}

                    {inputAuthorshipField &&
                        <SingleAuthorshipInputer
                            onClose={() => { setInputAuthorshipField(false) }}
                            onSave={(newAuthorship) => updateAuthorships(newAuthorship)}
                        />
                    }

                    {!inputAuthorshipField && <div >
                        <button style={{ margin: "20px 10px 10px 0" }} onClick={() => {
                            setInputAuthorshipField(true);
                        }} type="button" className="IB-btn "><FontAwesomeIcon icon={faPlus} /></button>
                        <span className="sublabel"  >Add an authorship</span>
                    </div>}
                </div>
            </div>
        </div>
    );
}

export default AuthorshipsInputer;


