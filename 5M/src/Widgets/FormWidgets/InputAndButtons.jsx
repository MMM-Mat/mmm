import React, { useState } from "react";
// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleInfo, faLocationDot, faLocationPin, faPlus, faMapPin, faCheck, faXmark, faTrash } from '@fortawesome/free-solid-svg-icons'


export function InputAndButtons({ placeholder, leftIcon, rightIcon, onClickLeft, onClickRight, data, disabled, value }) {
    const [newtext, setNewText] = useState(data);

    const dontSubmit = (e) => {
        console.log('Dont submit this');
    }

    let test;
    if (disabled) {
        test = false;
    }
    else if (data && data !== "") {
        test = true;
    }

    return (
        <div className="IB-container">
            {/* disabled={disabled? "true" : "false"} value={data} */}
            <div className="IB-quantity">
                <div className="IB-input-group">
                    <div className="IB-input-group-prepend">
                        <button name="cancel" onClick={onClickLeft ? onClickLeft : dontSubmit} type="cancel" className="IB-btn">
                            <FontAwesomeIcon icon={leftIcon ? leftIcon : faXmark} />
                        </button>


                    </div>
                    {/* CASE : disabled or without a value */}
                    {!test && <input style={{ color: "red" }} name="myInput" type="text" placeholder={placeholder} className="IB-form-control" data={data} disabled={disabled} />}

                    {/* CASE: not disabled and there is a value given */}
                    {test && <input style={{ color: "green" }} onChange={(event) => { setNewText(event.target.value) }} name="myInput" type="text" className="IB-form-control" data={data} value={newtext} />}



                    <div className="IB-input-group-append">
                        <button onClick={onClickRight ? onClickRight : () => { }} type="submit" className="IB-btn "><FontAwesomeIcon icon={rightIcon ? rightIcon : faCheck} /></button>
                    </div>
                </div>
            </div>
        </div>
    );
}


// https://codesandbox.io/embed/mq0xl9r83y