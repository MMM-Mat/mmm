// import React from 'react';
import LandmarkEditControl from './Buttons/LandmarkEditControl';

const LandmarkControls = ({ id, className }) => {

    return (<div className={className} style={{ display: "inline-block", marginTop: "10px" }}>
        <LandmarkEditControl  id={id} />
    </div>
    );
};

export default LandmarkControls;
