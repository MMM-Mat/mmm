import ELK from 'elkjs/lib/elk.bundled.js';
import React, { useCallback } from 'react';
import {
    useReactFlow,
} from 'reactflow';
// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowsUpDown, faArrowsLeftRight, faBox, faDharmachakra, faHeartPulse, faPuzzlePiece } from '@fortawesome/free-solid-svg-icons'

const elk = new ELK();

const useLayoutedElements = () => {
    const { getNode, getNodes, setNodes, getEdges, fitView } = useReactFlow();

    const defaultOptions = {
        'elk.algorithm': 'layered',
        'elk.layered.spacing.nodeNodeBetweenLayers': 100,
        // 'elk.algorithm': 'disco',
        'elk.spacing.nodeNode': 40,
        'separateConnectedComponents': true, // this is actually the default value
        'spacing.componentComponent': 70,
        'interactiveLayout': true,
        'elk.layered.nodePlacement.strategy': 'SIMPLE',
    };

    const getMMMNodes = () => {
        return getNodes().filter((n) => n.type === "mmmnode" || n.type === "mmmfile" || n.type === "mmmlink" || n.type === "mmmimage" || n.type === "mmmpen" || n.type === "mmmpit" || n.type === "childnode" || n.type === "mmmedgenode");

    }

    const getMMMEdgesToLayout = () => {
        return getEdges().filter((e) => {
            let source = getNode(e.source);
            let target = getNode(e.target);
            if (source === undefined) {
                console.log(e.id);
            }
            return e.type === "mmmedge" && source.type !== "mmmedge" && target.type !== "mmmedge"
        });

    }

    const getLayoutedElements = useCallback((options) => {
        const layoutOptions = { ...defaultOptions, ...options };
        const graph = {
            id: 'root',
            layoutOptions: layoutOptions,
            children: getMMMNodes(),
            edges: getMMMEdgesToLayout(),
        };
        console.log(graph);

        elk.layout(graph).then(({ children }) => {
            // By mutating the children in-place we saves ourselves from creating a
            // needless copy of the nodes array.
            children.forEach((node) => {
                node.position = {
                    x: node.x ?? 0,
                    y: node.y ?? 0
                };
            });
            setNodes(children);
            window.requestAnimationFrame(() => {
                fitView();
            });
        }, () => { console.log("rejected") });
    }, [fitView, setNodes]);

    return { getLayoutedElements };
};

const LayoutFlow = () => {
    const { getLayoutedElements } = useLayoutedElements();
    return (
        <div style={{ borderRadius: "3px", border: "solid 2px #848282" }}>
            <div style={{ margin: "5px" }}>Layout modes:</div>
            <div style={{ display: "block" }}>
                <button className="mybtn" style={{ margin: "5px" }}
                    onClick={() =>
                        getLayoutedElements({
                            'elk.algorithm': 'layered',
                            'elk.direction': 'UP',
                            // 'separateConnectedComponents': 'true', // this is actually the default value
                            // 'spacing.componentComponent': '350',
                            'interactiveLayout': 'true'
                        })
                    }><FontAwesomeIcon icon={faArrowsUpDown} /></button>

                <button className="mybtn" style={{ margin: "5px" }}
                    onClick={() =>
                        getLayoutedElements({ 'elk.algorithm': 'layered', 'elk.direction': 'RIGHT' })
                    }><FontAwesomeIcon icon={faArrowsLeftRight} /></button>


                <button title={"Box layout"} className="mybtn" style={{ margin: "5px" }}
                    onClick={() =>
                        getLayoutedElements({
                            'elk.algorithm': 'box',
                        })
                    }><FontAwesomeIcon icon={faBox} /></button>
            </div>
        </div>
    );
};

export default LayoutFlow;