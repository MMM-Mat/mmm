export const MMMnodeTypes = ["narrative", "question", "existence", "action", "data"];
export const MMMunidirectionalEdgeTypes = ["relatesto", "pertains", "answers", "questions", "nuances", "precedes", "pennedin", "substantiates", "instantiates", "supports"];
export const MMMadirectionalEdgeTypes = ["relate"];
export const MMMbidirectionalEdgeTypes = ["equates", "differsfrom"];
export const MMMpenTypes = ["default", "folder", "definition", "glossary"];

export function adjustEdgekind(kind, type) {
    let r;
    if (kind.includes("edge")) {
        if (MMMadirectionalEdgeTypes.includes(type)) { r = "adirectional edge"; }
        else if (MMMunidirectionalEdgeTypes.includes(type)) { r = "unidirectional edge"; }
        else if (MMMbidirectionalEdgeTypes.includes(type)) { r = "bidirectional edge"; }
        else {
            console.error("Type", type, "is not a type of", kind);
        }
    }
    else { r = kind; }
    return r;
}


export function getDefaultType(kind, initialkind, initialtype) {
    if (kind === simplifyKind(initialkind)) {
        return initialtype;
    }
    else { return defaultMMMTypes(kind) }
}

export function simplifyKind(kind) {
    if (kind.includes("edge")) { return "edge"; }
    else { return kind; }
}

export function defaultMMMTypes(kind) {
    if (kind === "node") { return "narrative"; }
    else if (kind === "pen") { return "default"; }
    else if (kind === "edge") { return "relate"; }
    else if (kind === "adirectional edge") { return "relate"; }
    else if (kind === "unidirectional edge") { return "relatesTo"; }
    else if (kind === "bidirectional edge") { return "equates"; }
}