export function printArrayAsString  (somearray) {
    if (somearray.length === 0) { return }
    let resultingstring = somearray[0];
    const [, ...rest] = somearray;
    for (const item of rest) {
        resultingstring += ", " + item
    }
    return resultingstring;
}

export function edgeMMMidToRFedgeID(id){
    return id+"-RF-edge";
}
function edgeIDsuffix () {
    return "-RF-edge";
}

export function edgenodeIDtoEdgeID(id){
    return id+edgeIDsuffix();
}

export function edgeIDtoEdgenodeID(id){
    return  id.substring(0, id.length - edgeIDsuffix().length);
}


function childIDsuffix (parentid) {
    return "-IN-" + parentid;
}

export function mmmidToContentID(childid, parentid) {
    return childid + childIDsuffix(parentid);
}

export function childIDtoOriginalID(childid,parentid) {
    let suffix = childIDsuffix(parentid);
    return  childid.substring(0, childid.length - suffix.length);
}

export function getActualTimestamp () {
    var currentdate = new Date();
    var minutes = currentdate.getMinutes();
    if (minutes<10){minutes="0"+minutes;}
    var datetime = currentdate.getDate() + "/"
      + (currentdate.getMonth() + 1) + "/"
      + currentdate.getFullYear() + " - "
      + currentdate.getHours() + ":"
      + minutes;
    //   + currentdate.getMinutes(); // + ":"
    //   + currentdate.getSeconds();
   return datetime;

}

export function getActualTeam () {
    return ["You"];
}

export function getActualAuthorship () {
    // console.log({timestamp: getActualTimestamp(), team:getActualTeam()});
    return {timestamp: getActualTimestamp(), team:getActualTeam()};
}

export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}