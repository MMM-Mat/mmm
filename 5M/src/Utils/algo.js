
export const getConnectedEdges = (nodes, edges) => {
    const nodeIds = nodes.map((node) => node.id);

    return edges.filter((edge) => nodeIds.includes(edge.source) || nodeIds.includes(edge.target));
};

export const getParents = (id,nodes) => {
    return nodes.filter((n) => n.type==="mmmpen" && n.data.contents.includes(id));
}
