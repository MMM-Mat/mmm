import React, { useContext, } from "react";
import { useReactFlow } from "reactflow";
import MMMNodeHandles from "./Handles";
import LandmarkLabel from "../Widgets/LandmarkLabel";
import LandmarkControls from "../Widgets/LandmarkControls";
import LandmarkInfoControl from "../Widgets/Buttons/LandmarkInfoControl";
// Contexts
import { WidgetsContext } from "../Contexts/WidgetsContextProvider";
// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFolder } from '@fortawesome/free-solid-svg-icons'
// Utils
// import { useEffectOnce } from "../Utils/useEffectOnce";
// let renderCount = 0;

const NodeNode = ({ id }) => {
    // renderCount += 1;
    // console.log(`${NodeNode.name}. (${id}) renderCount: `, renderCount);
    // useEffectOnce( ()=> {
    //     console.log('my NodeNode effect is running',id);
    //     return () => console.log('my NodeNode effect is destroying',id);
    // });

    const [form, setForm, onCloseForm, menu, setMenu, formDisabled, setFormDisabled, onCloseFormDisabled, selectingLandmarks, setSelectingLandmarks] = useContext(WidgetsContext);

    const { getNode } = useReactFlow();
    const node = getNode(id);
    let divBasicClassName = "mmm-" + node.data.type;
    if (node.selected) {
        divBasicClassName += " selectednode";
    }
    const islinknode = node.data.hasOwnProperty('tags') && node.data.tags.includes('file');
    const isimage = node.data.hasOwnProperty('tags') && node.data.tags.includes('imagenode');


    let divClassName = "mmmnode nodeshadow " + divBasicClassName;
    let filepath;
    if (islinknode) {
        divClassName += " linknode";
        // let mark=JSON.parse(node.data.marks[0]);
        if (typeof node.data.marks[0] === 'object') {
            let mark = node.data.marks.find((m) => typeof (m) === "object" && m.hasOwnProperty("localpath"))
            filepath = mark.localpath;
            // filepath= node.data.marks['localpath'];
        }
        else if (typeof node.data.marks[0] === 'string') {
            let mark = JSON.parse(node.data.marks[0]);
            filepath = mark.localpath;
        }


    }
    let imagebase64;
    // const getImageEncoding = async (filepath) => {
    //     console.log("filepath for encode", filepath);
    //     const res = await window.electronAPI.encodeImage(filepath);
    //     console.log(res);
    //     return res;
    // }

    if (isimage) {
        divClassName += " imagenode";
        let mark = JSON.parse(node.data.marks[0]);
        filepath = mark.localpath;
        // imagebase64 = getImageEncoding(filepath);
        // setImagebase64(getImageEncoding(filepath));
        imagebase64 = node.data.base64;
    }

    const openLocalFile = () => {
        window.electronAPI.openFile(filepath)
    }
    const showFileFolder = () => {
        window.electronAPI.showFileFolder(filepath)
    }

    const infocontrolstyle = { float: "right" };
    const infolabelstyle = { display: "flex", justifyContent: "space-between", };
    const otherstyle = { display: "flex" };

    return (
        <div className={divClassName}  >
            {!islinknode && !isimage && <LandmarkInfoControl className={divBasicClassName} style={infocontrolstyle} id={id} />}
            <div  >
                {!islinknode && <LandmarkLabel className="nodelabel" id={id} />}
                {/* LOCAL FILE : */}
                {islinknode && <div onClick={openLocalFile}> <LandmarkLabel className="nodelabel" id={id} /></div>}
                {/* IMAGE : */}
                {isimage && <img src={"data:image/jpeg;base64, " + imagebase64} width="200" />}

                <div style={infolabelstyle}>
                    {node.selected && <div style={otherstyle}>
                        {!islinknode && !isimage && <LandmarkControls className={divBasicClassName} id={id} />}
                        {islinknode &&
                            <div className={divBasicClassName} style={{ display: "inline-block", marginTop: "10px" }}>
                                <button title="Show in folder" className={divBasicClassName + " controlbutton dostuff"} onClick={showFileFolder} value="show in folder"><FontAwesomeIcon icon={faFolder} /></button>
                            </div>
                        }
                    </div>}
                </div>
            </div>
            <MMMNodeHandles />
        </div>
    );

};

export default NodeNode;
