import React, { useEffect, useCallback } from "react";
import {
  Position,
  useStore,
  getBezierPath,
  useReactFlow
} from "reactflow";
// Widgets
import LandmarkInfoControl from "../Widgets/Buttons/LandmarkInfoControl";
import MMMNodeHandles from "./BreakNodeHandles";
// Utils
import { edgeStyle } from "../Styles/MMMEdgeSetups";
import { edgeMMMidToRFedgeID } from "../Utils/SomeFunctions"
// import { useEffectOnce } from "../Utils/useEffectOnce";
// let renderCount = 0;

const contentStyle = {
  fontSize: 10,
  //   color: "#888899",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  //   textAlign: "center",
  // backgroundColor: "white",
  //   border: "1px solid #555",
  //   borderColor: "currentColor",
  borderRadius: 15,
  padding: "5px",
  maxWidth: "75px",
  //   width: EDGE_NODE_WIDTH,
  //   height: EDGE_NODE_HEIGHT
};

function handleIDtoPosition(hid) {
  switch (hid) {
    case 'top':
      return Position.Top;
    case 'bottom':
      return Position.Bottom;
    case 'left':
      return Position.Left;
    case 'right':
      return Position.Right;
    default:
      console.log(`Sorry, handleIDtoPosition is out of cases for ${hid}.`);
  }
}

function handlePositions(posX, posY, width, height, hid) {
  switch (hid) {
    case 'top':
      return [posX + width / 2, posY - height / 2];
    case 'bottom':
      return [posX + width / 2, posY + height];
    case 'left':
      return [posX, posY];
    case 'right':
      return [posX + width, posY];
    default:
      console.log(`Sorry, we are out of cases for ${hid}.`);
  }
}


const EdgeNode = ({ id, data }) => {
  // renderCount += 1;
  // // console.log(`${EdgeNode.name}. (${id}) renderCount: `, renderCount);
  // useEffectOnce(() => {
  //   console.log('my EdgeNode effect is running', id);
  //   return () => console.log('my EdgeNode effect is destroying', id);
  // });

  const { getNode, setNodes, setEdges } = useReactFlow();
  const edgenode = getNode(id);

  const [edgeCenterX, edgeCenterY] = useStore(
    useCallback(
      () => {
        const sourceNode = getNode(data.source);
        const targetNode = getNode(data.target);

        const [sourceX, sourceY] = handlePositions(sourceNode.position.x, sourceNode.position.y, sourceNode.width, sourceNode.height, data.sourcePosition);
        const [targetX, targetY] = handlePositions(targetNode.position.x, targetNode.position.y, targetNode.width, targetNode.height, data.targetPosition);

        const [_, centerX, centerY] = getBezierPath({
          sourceX: sourceX,
          sourceY: sourceY,
          targetX: targetX,
          targetY: targetY,
          sourcePosition: handleIDtoPosition(data.sourcePosition),
          targetPosition: handleIDtoPosition(data.targetPosition)
        });
        return [centerX, centerY];
      },
      [data.source, data.target, getNode]
    )
  );

  useEffect(() => {
    setNodes((nds) =>
      nds.map((node) => {
        if (node.id === id) {
          node.position = {
            x: edgeCenterX,
            y: edgeCenterY
          };
        }

        return node;
      })
    );
  }, [id, edgeCenterX, edgeCenterY]);

  useEffect(() => {
    setEdges((edges) =>
      edges.map((e) => {
        if (e.id === edgeMMMidToRFedgeID(id)) {
          e.data = { ...data };
          e.label = data.label;
          const { startArrow, endArrow  } = edgeStyle(e.data.type);
          e.markerStart = startArrow;
          e.markerEnd = endArrow;
        }
        return e;
      })
    );
  }, [id, data, setEdges]);


  let divClassName = "mmm-" + edgenode.data.type + "-breaknode ";
  let label = "";
  let altlabel;
  if (edgenode.data.hasOwnProperty('label')) {

    altlabel = edgenode.data.label.trim();
    if (altlabel.length > 0) {
      label = altlabel;
    }
  }

  return (
    <div className={"breaknode"} >
      <div className={divClassName} style={contentStyle}>
        {label.length > 0 && <div style={{ marginRight: "6px" }}>{label}</div>}
        <div ><LandmarkInfoControl id={id} style={{ float: "right", backgroundColor: "#ffffffa1", margin: "5px", borderRadius: "10px" }} /></div>
      </div>
      <MMMNodeHandles />
    </div>
  );
};

export default EdgeNode;
