import React from "react";
import {  Handle, Position } from "reactflow";


const MMMNodeHandles = ({className}) => {

    return (
        <div className={className} >
            <Handle
                id="top"
                type="source"
                position={Position.Top}
                className="childnode-handle"
                isConnectable={false}
            />

            <Handle
                id="bottom"
                type="source"
                position={Position.Bottom}
                className="childnode-handle"
                isConnectable={false}
            />
            <Handle
                id="left"
                position={Position.Left}
                type="source"
                className="childnode-handle"
                isConnectable={false}
            />
            <Handle
                id="right"
                type="source"
                position={Position.Right}
                className="childnode-handle"
                isConnectable={false}
            />

        </div>
    );
};

export default MMMNodeHandles;
