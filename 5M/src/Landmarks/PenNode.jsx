import React, { useEffect } from "react";
import { useReactFlow } from "reactflow";
import MMMNodeHandles from "./Handles";
// Widgets
import LandmarkControls from "../Widgets/LandmarkControls";
import LandmarkInfoControl from "../Widgets/Buttons/LandmarkInfoControl";
import { edgeStyle } from '../Styles/MMMEdgeSetups';
// Utils
import { sleep, edgeMMMidToRFedgeID, mmmidToContentID } from "../Utils/SomeFunctions";
// import { useEffectOnce } from "../Utils/useEffectOnce";
// let renderCount = 0;


function newChildNode(child, parentid, rank) {
    return {
        type: 'childnode',
        id: mmmidToContentID(child.id, parentid),
        position: {
            x: 0,
            y: 0
        },
        data: { ...child.data, rank: rank },
        origin: [0.5, 0.5],
        parentNode: parentid,
        extent: 'parent',
        connectable: false
    };
}

function newChildEdge(child, parentid, rank) {
    const { startArrow, endArrow, style } = edgeStyle(child.data.type);
    const newEdge = {
        type: 'childedge',
        id: mmmidToContentID(child.data.mmmid, parentid),
        data: { ...child.data, rank: rank },
        source: mmmidToContentID(child.source, parentid),
        target: mmmidToContentID(child.target, parentid),
        sourceHandle: child.sourceHandle,
        targetHandle: child.targetHandle,
        updatable: false,
        style: style,
    }
    newEdge.markerStart = startArrow;
    newEdge.markerEnd = endArrow;
    return newEdge;
}

const PenNode = ({ id }) => {
    // renderCount += 1;
    // console.log(`${PenNode.name}. (${id}) renderCount: `, renderCount);
    //   useEffectOnce( ()=> {
    //     console.log('my PenNode effect is running',id);
    //     return () => console.log('my PenNode effect is destroying',id);
    // });

    const { getNode, addNodes, addEdges, getEdge } = useReactFlow();
    const node = getNode(id);
    const contents = node.data.contents;
    let divBasicClassName = "mmm-pen";
    if (node.selected) {
        divBasicClassName += " selectednode";
    }
    let divClassName = "mmmnode nodeshadow " + divBasicClassName;

    let penContentsAsString = [];
    if (contents && contents.length > 0) {
        let childAsString;
        for (let i = 0; i < contents.length; i++) {
            let child = getNode(contents[i]);
            if (typeof child !== 'undefined') {
                childAsString = child.data.type + " " + child.data.kind;

                if (child.data.kind === "node") {
                    if (child.data.label.length > 50) {
                        childAsString += ': "' + child.data.label.substring(0, 25) + '..."';
                    }
                    else {
                        childAsString += ": " + child.data.label;
                    }
                }
                penContentsAsString.push(childAsString);
            }
            else {
                let child = getEdge(contents[i]);
                if (typeof child !== 'undefined') {
                    childAsString = child.data.type + " " + child.data.kind;
                    penContentsAsString.push(childAsString);
                }
            }
        }
    }

    useEffect(() => {
        sleep(0).then(() => {
            let children = 0;
            for (let i = 0; i < contents.length; i++) {
                let originalchild = getNode(contents[i]);
                if (typeof originalchild !== 'undefined' && originalchild.type !== "mmmedgenode") {
                    if (originalchild.type === "mmmnode" || originalchild.type === "mmmpen") {
                        addNodes(newChildNode(originalchild, id, children));
                        children++;
                    }
                    else if (originalchild.type === "mmmpen") {
                        addNodes(newChildNode(originalchild, id, children)); // todo: childpen
                        children++;
                    }
                    else {
                        console.log("found an unexpected child ", originalchild.type, originalchild.data.kind, originalchild);
                    }
                }
                else {
                    let originalchild = getEdge(edgeMMMidToRFedgeID(contents[i]));
                    if (typeof originalchild !== 'undefined') {
                        addEdges(newChildEdge(originalchild, id, i));
                    }
                    else {
                        console.log("found an undefined child ", contents[i]);
                    }
                }
            }
        });
    }, [id, contents]);
    // , getNode, addNodes, addEdges, getEdge

    return (
        <div className={divClassName}  >
            <LandmarkInfoControl className={divBasicClassName} style={{ float: "right" }} id={id} />
            <div className={"type-label mmm-pen"} style={{ backgroundColor: "transparent" }}>{node.data.type + " pen"}</div>

            <div style={{ width: "400px", height: "150px" }}></div>

            {contents && <div className="nodelabel" style={{ textAlign: "left" }}>
                Contents:
                <ul style={{ fontSize: "small", marginTop: "0px" }}>
                    {penContentsAsString.map((item, index) => (<li key={index}>{item}</li>))}
                </ul>
            </div>}

            <div  >
                <div style={{ display: "flex", justifyContent: "space-between", }}>
                    {node.selected && <div style={{ display: "flex" }}>
                        <LandmarkControls className={divBasicClassName} id={id} />
                    </div>}
                </div>
            </div>
            <MMMNodeHandles />
        </div>
    );
};

export default PenNode;
