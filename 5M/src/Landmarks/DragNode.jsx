
const DragNode = ({mmmtype}) => {

  const cssclass = "protommmnode mmm-" + mmmtype;

  return (
    <div className="protocontainer">
    <div className={cssclass}>
        <div className="protommmnode-type-label" style={{fontSize: "small"}}>New {mmmtype} </div>
      </div>
    </div>
  );
};

export default DragNode;