import React, { useCallback, useState } from "react";
import { useReactFlow } from "reactflow";
// Widgets
import TypeSelector from "../Widgets/Subwidgets/TypeSelectorProto";
import ProtoCancelButton from "../Widgets/Buttons/CancelProto";
import ProtoSaveButton from "../Widgets/Buttons/SaveProto";
import LandmarkSelector from "../Widgets/Subwidgets/LandmarkSelector";

const ProtoPen = ({ id }) => {
    const { getNode, setNodes } = useReactFlow();
    const node = getNode(id);
    const [selecting, setSelecting] = useState(false);
    const [contents, setContents] = useState([]);

    const saveContents = () => {
        setSelecting(false);
        setNodes((nds) =>
            nds.map((node) => {
                if (node.id === id) {
                    node.data.contents = contents;
                }
                return node;
            })
        );
    }

    const divClassName = "mmmnode nodeshadow mmm-pen";

    const handleTypeChange = useCallback(
        (newtype) => {
            setNodes((nds) =>
                nds.map((node) => {
                    if (node.id === id) {
                        node.data = { ...node.data, type: newtype };
                    }
                    return node;
                })
            );
            return 0;
        },
        [id, setNodes]
    );

    const toggleSelection = () => {
        if (selecting) {
            saveContents();
            setSelecting(!selecting);
        }
        else {
            setNodes((nodes) => nodes.map((n) => ({ ...n, selected: false })));
            setSelecting(!selecting);
            console.log("setcontents=", setContents, "contents=", contents, "selecting", selecting);
        }
    }
    return (
        <div>
            <div className={divClassName} style={{ minWidth: "200px" }} >
                <div
                    style={{ display: "flex", justifyContent: "space-between" }}>
                    <TypeSelector
                        className={"newnodetypeselector mmm-pen"}
                        style={{
                            fontSize: "small",
                            fontWeight: "bold",
                            fontVariant: "small-caps",
                            textTransform: "capitalize",
                        }}
                        mmmkind={node.data.kind}
                        mmmtype={node.data.type}
                        onChange={handleTypeChange}
                    />
                    <ProtoCancelButton id={id} className={"mmm-pen"} />
                </div>

                {selecting && <LandmarkSelector different={id} setSelected={setContents} />}

                {selecting && <p>Selected:
                    <ul style={{ fontSize: "small", marginTop: "0px" }}>
                        {contents.map((item, index) => (<li key={index}>{item}</li>))}
                    </ul></p>}

                {!selecting && <p>Pen contents:
                    <ul style={{ fontSize: "small", marginTop: "0px" }}>
                        {node.data.contents.map((item, index) => (<li key={index}>{item}</li>))}
                    </ul></p>}

                <div style={{ marginTop: "20px" }}>
                    <button className={"mybutton mybtn"} onClick={toggleSelection}> {selecting ? "save selection" : "Select"} </button>
                    <ProtoSaveButton kind="pen" id={id} className={"mmm-pen"} />
                </div>
            </div>
        </div>


    );
};

export default ProtoPen;
