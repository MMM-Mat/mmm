import React, { useEffect, useCallback} from "react";
import useViewportSize from 'react-hook-viewport-size';
import {
    useReactFlow,
    useStore,
} from "reactflow";
import MMMNodeHandles from "./ChildNodeHandles";
import LandmarkInfoControl from "../Widgets/Buttons/LandmarkInfoControl";
// Utils
// import { useEffectOnce } from "../Utils/useEffectOnce";
// let renderCount = 0;

const ChildNode = ({ id }) => {
    // renderCount += 1;
    // console.log(`${ChildNode.name}. (${id}) renderCount: `, renderCount);
    // useEffectOnce( ()=> {
    //     console.log('my ChildNode effect is running',id,);
    //     return () => console.log('my ChildNode effect is destroying',id);
    // });
    
    const { getNode, setNodes } = useReactFlow();
    const node = getNode(id);
    const { setViewport } = useReactFlow();
    const [viewportWidth, viewportHeight] = useViewportSize()
    const parentNode = getNode(node.parentNode);

    // Get the coordinates of the EdgeNode:
    const pos = useStore(
        useCallback(
            () => {
                return parentNode.position;
            },
            [parentNode.position]
        )
    );

    // Assign these coordinates to the RF node:
    useEffect(() => {
        setNodes((nds) =>
            nds.map((node) => {
                if (node.id === id) {
                    node.position = {
                        x: node.data.rank * 70 + 10,
                        y: node.data.rank * 40 + 50
                    }
                }
                return node;
            })
        );
    }, [id, pos, setNodes]);

    // To find the original child node outside the pen:
    const focusViewport = (event) => {
        console.log("event", event);
        let orginalID = node.data.mmmid;
        let originalnode = getNode(orginalID);
        console.log("original ID", orginalID, "label:",node.data.label.slice(0, 20),"Original node", originalnode, originalnode.position,originalnode.data.label.slice(0,20));
        originalnode.selected=true;
        setViewport(
            {
                x: -originalnode.position.x - (originalnode.width) + (viewportWidth / 2), 
                y: -originalnode.position.y - (originalnode.height) + (viewportHeight / 2),
                zoom: 1.2,
            },
            { duration: 800 }
        );
    }

    let cssclass = "childnode mmm-";
    let label ;
    if (node.data.kind==="node"){
        cssclass = "childnode mmm-"+node.data.type;
        label = node.data.label;
    }
    else if (node.data.kind==="pen"){
        cssclass = "childnode mmm-pen";
        label = node.data.type + " pen";
    }
    return (
        <div className={cssclass} onClick={focusViewport} >
            <LandmarkInfoControl style={{ float: "right" }} id={id} />
            <div className="nodelabel" > {label} </div>
            <MMMNodeHandles />
        </div>
    );
};

export default ChildNode;