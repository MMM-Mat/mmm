import React, { useCallback, useState } from "react";
import { useReactFlow } from "reactflow";
// Widgets
import LandmarkLabel from "../Widgets/LandmarkLabel";
import TypeSelector from "../Widgets/Subwidgets/TypeSelectorProto";
import ProtoCancelButton from "../Widgets/Buttons/CancelProto";
import ProtoSaveButton from "../Widgets/Buttons/SaveProto";
// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faImage, faFile } from '@fortawesome/free-solid-svg-icons'


const ProtoNode = ({ id }) => {
    const { getNode, setNodes } = useReactFlow();
    const node = getNode(id);

    const divClassName = "mmmnode nodeshadow mmm-" + node.data.type;
    const style = {}

    const handleTypeChange = useCallback(
        (newtype) => {
            setNodes((nds) =>
                nds.map((node) => {
                    if (node.id === id) {
                        node.data = { ...node.data, type: newtype };
                    }
                    return node;
                })
            );
            return 0;
        },
        [id, setNodes]
    );

    const [imagebase64, setImagebase64] = useState("unknown");
    const [ifilepath, setIFilepath] = useState("unknown");
    const [imageon, setImageOn] = useState(false);

    const handleImagePick = async () => {
        const response = await window.electronAPI.pickImage();
        setImagebase64(response[1]); // `data:image/jpg;base64,${response[0]}`
        setIFilepath(response[0]);
        setImageOn(true);
        setNodes((nds) =>
            nds.map((node) => {
                if (node.id === id) {
                    var filename =  response[0].substring(response[0].lastIndexOf("\\") + 1);
                    filename = filename.replace(/^.*[\\/]/, '');
                    node.data.label = filename;

                    node.data.tags = ["imagenode"];
                    let mark = {
                        localpath: response[0]
                    }
                    node.data.marks = [JSON.stringify(mark)];
                    node.data.base64 = response[1];
                }
                return node;
            })
        );
    }

    const [filepath, setFilepath] = useState("unknown");
    const [fileon, setFileOn] = useState(false);

    const handleFilePick = async () => {
        const fp = await window.electronAPI.pickFile()
        setFilepath(fp);
        setFileOn(true);
        setNodes((nds) =>
            nds.map((node) => {
                if (node.id === id) {
                    var filename =  fp.substring(fp.lastIndexOf("\\") + 1);
                    filename = filename.replace(/^.*[\\/]/, '');
                    node.data.label = filename;
                    node.data.tags = ["file"];
                    let mark = {
                        localpath: fp
                    }
                    node.data.marks = [JSON.stringify(mark)];
                }
                return node;
            })
        );
    }

    const openLocalFile = () => {
        window.electronAPI.openFile(filepath)
    }

    return (
        <div>
            <div className={divClassName} >
                <div
                    className={"mmm-" + node.data.type}
                    style={{ display: "flex", justifyContent: "space-between" }}>
                    <TypeSelector
                        className={"newnodetypeselector mmm-" + node.data.type}
                        style={{
                            fontSize: "small",
                            fontWeight: "bold",
                            fontVariant: "small-caps",
                            textTransform: "capitalize",
                        }}
                        mmmkind={node.data.kind}
                        mmmtype={node.data.type}
                        onChange={handleTypeChange}
                    />
                    <ProtoCancelButton id={id} />
                </div>
                {/* IMAGE: */}
                {imageon && <img src={"data:image/jpeg;base64, " + imagebase64} width="200" />}
                {/* FILE */}
                {fileon && <div onClick={openLocalFile}> <LandmarkLabel className="nodelabel" id={id} /></div>}
                {/* LABEL */}
                {!imageon && !fileon && <LandmarkLabel style={style} className="emptynodelabel" placeholder="Type a label here." id={id} />}

                <div className={"mmm-" + node.data.type} style={{ display: "flex", justifyContent: "space-between", marginTop: "20px", }}>
                    <div style={{ display: "flex" }}>
                        {/* SELECT IMAGE */}
                        <button onClick={handleImagePick}
                            title="Select an image"
                            className={" controlbutton dostuff"} >
                            <FontAwesomeIcon icon={faImage} />
                        </button>
                        {/* SELECT FILE */}
                        {node.data.type === "existence" && <button onClick={handleFilePick}
                            title="Select a file"
                            className={" controlbutton dostuff"} >
                            <FontAwesomeIcon icon={faFile} />
                        </button>}
                    </div>
                    <ProtoSaveButton kind="node" id={id} />
                </div>

            </div>


        </div>
    );
};

export default ProtoNode;
