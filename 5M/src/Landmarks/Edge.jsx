import React, { useEffect } from "react";
import {
    useReactFlow, getBezierPath
} from 'reactflow';
import { edgeStyle, markerToString } from '../Styles/MMMEdgeSetups';
// Utils
// import { useEffectOnce } from "../Utils/useEffectOnce";
// let renderCount = 0;

export default function Edge(props) {


    const {
        id,
        // source,
        // target,
        sourceX,
        sourceY,
        targetX,
        targetY,
        sourcePosition,
        targetPosition,
        // markerStart,
        // markerEnd,
        data,
    } = props;

    // useEffectOnce( ()=> {
    //     console.log('my Edge effect is running',id);
    //     return () => console.log('my Edge effect is destroying',id);
    // });
    // renderCount += 1;
    // console.log(`${Edge.name}. (${id}) renderCount: `, renderCount);


    const [edgePath ] = getBezierPath({
        sourceX,
        sourceY,
        sourcePosition,
        targetX,
        targetY,
        targetPosition,
    });

    const { getEdge, addNodes } = useReactFlow();

    // Add EdgeNode
    useEffect(() => {
        const newNode = {
            type: 'mmmedgenode',
            id: data.mmmid,
            position: {
                x: 0,
                y: 0
            },
            // draggable: false,
            data: { ...data, sourcePosition, targetPosition },
            // origin: [-1, 0.5],
        };
        addNodes(newNode);
    }, [data, id]);

    const edge = getEdge(id);
    const { startArrow, endArrow, style } = edgeStyle(edge.data.type);
    edge.markerStart = startArrow;
    edge.markerEnd = endArrow;

    if (edge.selected) {

        console.log("selected edge:", edge);
    }

    return (
        <>
            <path
                id={id}
                className="react-flow__edge-path"
                d={edgePath}
                style={style}
                markerStart={markerToString(edge.markerStart)}
                markerEnd={markerToString(edge.markerEnd)}
            />
        </>
    );
}
