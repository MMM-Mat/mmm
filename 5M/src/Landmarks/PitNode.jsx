import React from "react";
import MMMNodeHandles from "./Handles";
// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faRadiation, faSkull, faFaceDizzy } from '@fortawesome/free-solid-svg-icons'


const PitNode = () => {
    return (
            <div className="tooltip mmm-pit inverse"
                style={{
                    fontSize: "50px",
                    marginTop: "-5px",
                    marginBottom: "-5px",
                    // borderRadius: "100%",
                    cursor: "pointer"
                }}>
                <FontAwesomeIcon icon={faFaceDizzy} />

                <span className="tooltiptext"
                    style={{
                        fontSize: "18px",
                        padding: "2px",
                        margin: "5px",
                        color: "black",
                        backgroundColor: "white",
                        minWidth: "120px",
                    }}>
                    The Pit !
                </span>
            <MMMNodeHandles />
        </div>
    );
};

export default PitNode;
