import React from "react";
import {  Handle, Position } from "reactflow";


const MMMNodeHandles = ({className}) => {

    return (
        <div className={className} >
            <Handle
                id="top"
                isConnectable="true"
                type="source"
                position={Position.Top}
                className="breaknode-handle"
            />

            <Handle
                id="bottom"
                type="source"
                isConnectable="true"
                position={Position.Bottom}
                className="breaknode-handle"
            />
            {/* <Handle
                id="left"
                position={Position.Left}
                type="source"
                className="mmmnode-side-handle"
            />
            <Handle
                id="right"
                type="source"
                position={Position.Right}
                className="mmmnode-side-handle"
            /> */}

        </div>
    );
};

export default MMMNodeHandles;
