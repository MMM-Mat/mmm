import React, { useCallback, useState, useEffect } from 'react';
import ReactFlow, {
    Controls,
    updateEdge,
    addEdge,
    applyEdgeChanges,
    applyNodeChanges,
    Panel
} from 'reactflow';
// LANDMARKS
import NodeNode from './Landmarks/NodeNode.jsx';
import PitNode from './Landmarks/PitNode.jsx';
import PenNode from './Landmarks/PenNode.jsx';
import Edge from './Landmarks/Edge.jsx';
import EdgeNode from './Landmarks/EdgeNode.jsx';
import ProtoNode from './Landmarks/ProtoNode.jsx';
import ProtoPen from './Landmarks/ProtoPen.jsx';
import ChildNode from './Landmarks/ChildNode.jsx';
// UTILS
import { edgeStyle } from './Styles/MMMEdgeSetups';
import { getActualTimestamp, getActualAuthorship, edgeMMMidToRFedgeID } from './Utils/SomeFunctions';

// WIDGETS
import LegendPanel from './Widgets/LegendPanel';
import ControlsPanel from './Widgets/ControlsPanel.jsx';
import ContextMenu from './Widgets/LandmarkContextMenu.jsx';
import { WidgetsContext } from './Contexts/WidgetsContextProvider.js';
import LandmarkInfoForm from './Widgets/LandmarkInfoForm.jsx';
// STYLES
import './Styles/mmm.css';
import './Styles/practical.css';
import 'reactflow/dist/style.css';
import './Styles/edgestyles.css';
import './Styles/infoform.css';
// DATA
import {
    initialNodes as initialNodes,
    initialEdges as initialEdges,
} from "./Data/InitialData.js";
// REACT FLOW TYPES
const nodeTypes = {
    mmmpit: PitNode,
    mmmnode: NodeNode,
    mmmpen: PenNode,
    protonode: ProtoNode,
    protopen: ProtoPen,
    childnode: ChildNode,
    mmmedgenode: EdgeNode,
};
const edgeTypes = {
    mmmedge: Edge,
    childedge: Edge
};

// INIT
let id = 0;
const getId = () => `${id++}-${Date.now()}`;

const Flow = () => {

    const [nodes, setNodes] = useState(initialNodes);
    const [edges, setEdges] = useState(initialEdges);
    const onNodesChange = useCallback(
        (changes) => setNodes((nds) => applyNodeChanges(changes, nds)),
        [],
    );
    const onEdgesChange = useCallback((changes) => {
        setEdges((eds) => applyEdgeChanges(changes, eds))
    }, [],);

    // gets called after end of edge gets dragged to another source or target
    const onReconnectEdge = useCallback(
        (oldEdge, newConnection) => {
            const newData = { ...oldEdge.data, source: newConnection.source, target: newConnection.target, sourcePosition: newConnection.sourceHandle, targetPosition: newConnection.targetHandle };
            setNodes((nds) =>
                nds.map((node) => {
                    if (node.id === newData.mmmid) {
                        // node.data.source = newConnection.source;
                        // node.data.target = newConnection.target;
                        node.data = newData
                    }
                    return node;
                })
            );
            setEdges((els) => updateEdge(
                {
                    ...oldEdge,
                    data: newData
                }, newConnection, els))
        },
        []
    );
    const onCreateNewEdge = useCallback(
        (connection) => {
            const newedgetype = 'relatesto';
            const newedgekind = 'unidirectional edge';
            const newId = "E-" + getId() + "-" + connection.source + "-->" + connection.target;
            const newData = {
                type: newedgetype,
                mmmid: newId,
                source: connection.source,
                target: connection.target,
                kind: newedgekind,
            };
            const { startArrow, endArrow, style } = edgeStyle(newedgetype);
            console.log("endArrow=", endArrow);
            const newEdge = {
                ...connection,
                id: edgeMMMidToRFedgeID(newId),
                type: 'mmmedge',
                data: newData,
                // markerEnd: getMarker(newedgetype),
                // markerStart: getMarker(newedgetype),
                // markerEnd: endArrow,
                // markerStart: startArrow,
            };
            console.log("new edge=", newEdge);
            setEdges((oldedges) => addEdge(newEdge, oldedges));
        },
        [setEdges, setNodes]
    );

    // ******************************************** 
    // **                                        **
    // **             CONTEXT MENU               **
    // **                                        **
    // ********************************************
    const [position, setPosition] = useState({ x: 0, y: 0 });
    const [menu, setMenu] = useState(null);
    const onNodeContextMenu = useCallback(
        (event, node) => {
            // Prevent native context menu from showing
            if (!node.type.includes("proto") && node.type != "mmmpit" && node.type != "childnode") {
                event.preventDefault();
                setPosition({ x: event.clientX, y: event.clientY });
                setMenu({
                    id: node.id,
                    data: node.data,
                    left: event.clientX,
                    top: event.clientY,
                });
            }
        },
        [setMenu]
    );
    // Close the context menu if it's open whenever the window is clicked.
    const onPaneClick = useCallback(() => { setMenu(null) }, [setMenu]);

    // ******************************************** 
    // **                                        **
    // **          INFORMATION FORM              **
    // **                                        **
    // ********************************************
    const [form, setForm] = useState(null);
    const onCloseForm = useCallback(() => { setForm(null); }, [setForm]);

    const [formDisabled, setFormDisabled] = useState(null);
    const onCloseFormDisabled = useCallback(() => { setFormDisabled(null); }, [setFormDisabled]);

    const [selectingLandmarks, setSelectingLandmarks] = useState(null);
    const onStopSelecting = useCallback(() => {
        let array = selectingLandmarks.selected;
        setSelectingLandmarks(null); return array;
    }, [selectingLandmarks]);
    const onStartSelecting = useCallback(
        (event) => {
            // event.preventDefault();
            setSelectingLandmarks({
                selected: []
            });
        },
        [setSelectingLandmarks]
    );

    // ******************************************** 
    // **                                        **
    // **      DRAG AND DROP LEGEND NODES        **
    // **                                        **
    // ******************************************** 
    const [reactFlowInstance, setReactFlowInstance] = useState(null);

    const onDragOver = useCallback((event) => {
        event.preventDefault();
        event.dataTransfer.dropEffect = 'move';
    }, []);

    const onDrop = useCallback(
        (event) => {
            event.preventDefault();

            const inputtype = event.dataTransfer.getData('application/reactflow');
            // check if the dropped element is valid
            if (typeof inputtype === 'undefined' || !inputtype) {
                return;
            }
            const [kind, type, mmmtype] = inputtype.split(' ');

            const position = reactFlowInstance.screenToFlowPosition({
                x: event.clientX,
                y: event.clientY,
            });
            const id = getId();
            const newNode = {
                id: id,
                type,
                position,
                data: {
                    id: id,
                    type: mmmtype,
                    kind: kind,
                    status: "private",
                    timestamp: getActualTimestamp(),
                    authorships: [getActualAuthorship()]
                },
                origin: [0.5, 0.5]
            };
            if (kind === "node") {
                newNode.data.label = "";
            }
            if (kind === "pen") {
                newNode.data.contents = [];
            }
            setNodes((nds) => nds.concat(newNode));
        },
        [setNodes, reactFlowInstance],
    );

    const onInit = (instance) => {
        ; setReactFlowInstance(instance)
    }

    return (
        <WidgetsContext.Provider value={[form, setForm, onCloseForm, menu, setMenu, formDisabled, setFormDisabled, onCloseFormDisabled, selectingLandmarks, setSelectingLandmarks, onStartSelecting, onStopSelecting]}>
            <div className="App">
                <div style={{ width: '100vw', height: '100vh' }}>
                    <ReactFlow id="flow-1"
                        nodes={nodes}
                        edges={edges}
                        nodeTypes={nodeTypes}
                        edgeTypes={edgeTypes}
                        onNodesChange={onNodesChange}
                        onEdgesChange={onEdgesChange}
                        snapToGrid
                        connectionMode='loose'
                        onEdgeUpdate={onReconnectEdge}
                        onConnect={onCreateNewEdge}
                        onInit={onInit}
                        onDrop={onDrop}
                        onDragOver={onDragOver}
                        onNodeContextMenu={onNodeContextMenu}
                        onPaneClick={onPaneClick}
                        minZoom={0.1}
                        maxZoom={2.5}
                        defaultViewport={{ x: 0, y: 0, zoom: 1 }}
                        fitView={false}
                    //   attributionPosition="top-right"
                    >
                        <Controls />
                        <LegendPanel />
                        <ControlsPanel />
                        {menu && <ContextMenu onClick={onPaneClick}  {...menu} />}
                        {form && <Panel position="top-left"> <LandmarkInfoForm /></Panel>}
                    </ReactFlow>
                </div></div>
        </WidgetsContext.Provider>
    );
};


const App = () => {
    return <Flow />
}


export default App;
