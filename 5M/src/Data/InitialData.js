const pitdata = { kind: "pit", mmmid: "0", status: "public" };
const pitnode = { id: pitdata.mmmid, type: "mmmpit", data: pitdata, position: { x: window.innerWidth * 1.85 , y: window.innerHeight * 1.75  } }

export const initialNodes = [
    pitnode
];

export const initialEdges = [
];