import { MMMEdgeColorCodes } from './charteGraphique';
import { MarkerType } from "reactflow";

let pertainsMarker = {
    type: MarkerType.ArrowClosed,
    width: 7,
    height: 7,
    color: MMMEdgeColorCodes("pertains"),
    strokeWidth: 7
};

export const  Markers=[pertainsMarker];