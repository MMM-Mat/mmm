import { MarkerType } from "reactflow";
import { MMMEdgeColorCodes } from './charteGraphique';
import { adjustEdgekind } from "../Utils/MMMfields";


export function getMarker(typeName) {
    let kind = adjustEdgekind('edge', typeName);
    if (kind === "adirectional edge") { return null; }
    else if (kind === "unidirectional edge") {
        let color = MMMEdgeColorCodes(typeName);
        return {
            type: MarkerType.ArrowClosed,
            width: 7,
            height: 7,
            color: color,
            strokeWidth: 7
        };
    }
    else if (kind === "bidirectional edge") {
        let color = MMMEdgeColorCodes(typeName);
        return {
            type: MarkerType.ArrowClosed,
            width: 7,
            height: 7,
            color: color,
        };
    }
}

export function markerToString(marker) {
    if (marker == null) { return "url('#')"; }
    return "url('#flow-1__color=" + marker.color + "&height=7&type=" + marker.type + "&width=7')";
}

export function basicAEdge(typeName, alternativeTypeNames) {
    let color = MMMEdgeColorCodes(typeName);
    return {
        typeName: typeName,
        alternativeTypeNames: alternativeTypeNames,
        style: {
            strokeWidth: 1,
            stroke: color,
        },
        startArrow: {},
        endArrow: {}
    };
}


export function basicUniEdge(typeName, alternativeTypeNames) {
    let color = MMMEdgeColorCodes(typeName);
    return {
        typeName: typeName,
        alternativeTypeNames: alternativeTypeNames,
        // animated: true,
        startArrow: {},
        endArrow: {
            type: MarkerType.ArrowClosed,
            width: 7,
            height: 7,
            color: color,
        },
        style: {
            strokeWidth: 5,
            stroke: color,
        }

    };
}

export function basicBiEdge(typeName, alternativeTypeNames) {
    let color = MMMEdgeColorCodes(typeName);
    let w;
    let h;
    let s;
    if (typeName === "relatesto") {
        w = 17;
        h = 17;
        s = 2;
    }
    else {
        w = 7;
        h = 7;
        s = 5;
    }

    return {
        typeName: typeName,
        alternativeTypeNames: alternativeTypeNames,
        // animated: true,
        startArrow: {
            type: MarkerType.ArrowClosed,
            width: w,
            height: h,
            color: color,
        },
        endArrow: {
            type: MarkerType.ArrowClosed,
            width: w,
            height: h,
            color: color,
        },
        style: {
            strokeWidth: s,
            stroke: color,
        }
    };
}

export function edgeStyle(inputTypeName) {
    let typeName = inputTypeName.toLowerCase();
    switch (typeName) {
        case "pertains": { let s = basicUniEdge("pertains", ["details", "characterizes"]); return s; }
        case "nuances": {
            let s = basicUniEdge("nuances", ["invalidates", "contradicts"]); s.style.strokeWidth = 3;
            s.style.strokeDasharray = 1; return s;
        }
        case "supports": { let s = basicUniEdge("supports", ["proves"]); return s; }
        case "questions": { let s = basicUniEdge("questions", ["challenges", "requires precisions"]); return s; }
        case "substantiates": { let s = basicUniEdge("substantiates", ["represented by", "symbolised by", "named"]); return s; }
        case "instantiates": { let s = basicUniEdge("instantiates", ["exemplifies", "is-a", "is a type of"]); return s; }
        case "answers": { let s = basicUniEdge("answers", []); return s; }
        case "relatesto": {
            let s = basicUniEdge("relatesto", ["default"]); s.style.strokeWidth = 2; return s;
        }
        case "relate": { let s = basicAEdge("relate", ["default"]); return s; }
        case "equates": { let s = basicBiEdge("equates", ["synonymous"]); return s; }
        case "differsfrom": {
            let s = basicBiEdge("differsfrom", ["as opposed to"]); s.style.strokeWidth = 3;
            s.style.strokeDasharray = 1; return s;
        }
        default:
            console.log(`Sorry, we are out of cases for ${typeName}.`);

    }
}
