export function MMMEdgeColorCodes(mmmtypename) {
    switch (mmmtypename) {
        case 'pertains': return "#963484";
        case 'mmm-pertains': return "#963484";
        case 'nuances': return "#e60000";
        case 'questions': return "#ff751a";
        case 'instantiates': return "#009999";
        case 'substantiates': return "#0099cc";
        case 'precedes': return "#ffccff";
        case 'supports': return "#ffcc00";
        case 'pennedin': return "#7ecd7e";
        case 'answers': return "#ffcc00";
        case 'relatesto': return "#cccccc";
        case 'relate': return "#bfbfbf";
        case 'equates': return "#3066BE";
        case 'differsfrom': return "#e60000";
    }
}
