# 5M Application

This is a demo of a generic graphical MMM editor application. It implements the specifications of the "Generic Visual Editor" described in [the 2020 proposal](https://www.incaseofpeace.org/documents/planMMMproposal.pdf) ([page 69](https://www.incaseofpeace.org/documents/planMMMproposal.pdf#page=84)).

