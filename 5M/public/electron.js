// Module to control the application lifecycle and the native browser window. Inspired by https://mmazzarolo.com/blog/2021-08-12-building-an-electron-application-using-create-react-app/
const { app, BrowserWindow, protocol, ipcMain, dialog, shell } = require("electron");
const path = require("path");
const url = require("url");
const fs = require('fs');

/******************************************** 
/**                                        **
/**                                        **
/**                                        **
/********************************************/
function handleSetTitle(event, title) {
    const webContents = event.sender
    const win = BrowserWindow.fromWebContents(webContents)
    win.setTitle(title)
}
async function openLocalFile(event, filepath) {
    shell.openPath(filepath);
}
async function showFileFolder(event, filepath) {
    shell.showItemInFolder(filepath);
}
async function trashFile(event, filepath) {
    shell.trashItem(filepath);
}
async function startProgram(event, name) {
    let extension;
    if (name === "word") {
        extension = "docx"
    }
    let filenam = "test." + extension;
    shell.openItem(path.join(__dirname, filename));
}

async function handlePickFile() {
    const { canceled, filePaths } = await dialog.showOpenDialog()
    if (!canceled) {
        return filePaths[0]
    }
}

// function to encode file data to base64 encoded string
function base64_encode(file) {
    // read binary data
    var bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return new Buffer(bitmap).toString('base64');
}

async function handlePickImage() {
    const { canceled, filePaths } = await dialog.showOpenDialog({
        properties: ["openFile"],
        message: "Select an image please",
        filters: [
            { name: 'Images', extensions: ['jpg', 'png', 'gif'] },
        ]
    })
    if (!canceled) {
        var base64 = base64_encode(filePaths[0]);
        return [filePaths[0], base64];
    }
}

const encodeImage = async (event,data) => {
    const res = base64_encode(data);
    // return base64_encode(event[0]);
    return res;
}

/******************************************** 
/**                                        **
/**            SPLASH SCREEN               **
/**                                        **
/********************************************/
let splashWindow;

function createSplashWindow() {
    splashWindow = new BrowserWindow({
        // width: 400,
        // height: 300,
        transparent: true,
        frame: false,
        alwaysOnTop: true,
        center: true,
        resizable: false,
        webPreferences: {
            nodeIntegration: true
        }
    });
    splashWindow.maximize();
    splashWindow.loadFile(path.join(__dirname, 'splash.html'));
    splashWindow.on('closed', () => {
        splashWindow = null;
    });
}

/******************************************** 
/**                                        **
/**                                        **
/**                                        **
/********************************************/
// Create the native browser window.
function createWindow() {
    const mainWindow = new BrowserWindow({
        // type: 'desktop',
        // minWidth: 800,
        // minHeight: 600,
        // resizable: true,
        // Set the path of an additional "preload" script that can be used to communicate between node-land and browser-land.
        webPreferences: {
            preload: path.join(__dirname, "preload.js"),
        },
    });

    mainWindow.maximize();

    ipcMain.on('set-title', handleSetTitle);
    ipcMain.handle('openFile', openLocalFile);
    ipcMain.handle('showFileFolder', showFileFolder);
    ipcMain.handle('startProgram', startProgram);
    ipcMain.handle('trashFile', trashFile);
    ipcMain.handle('encodeImage', encodeImage);
    ipcMain.handle('dialog:pickFile', handlePickFile);
    ipcMain.handle('dialog:pickImage', handlePickImage);
    ipcMain.handle("showDialog", (e, message) => {
        dialog.showMessageBox(mainWindow, { message });
    });

    // In production, set the initial browser path to the local bundle generated
    // by the Create React App build process.
    // In development, set it to localhost to allow live/hot-reloading.
    const appURL = app.isPackaged
        ? url.format({
            pathname: path.join(__dirname, "index.html"),
            protocol: "file:",
            slashes: true,
        })
        : "http://localhost:3000";
    // mainWindow.loadURL(appURL);
    mainWindow.loadFile(path.join(__dirname, 'splash.html'));
    setTimeout(() => {
        mainWindow.loadURL(appURL);
    }, 1500);


    // Automatically open Chrome's DevTools in development mode.
    if (!app.isPackaged) {
        mainWindow.webContents.openDevTools();
    }
}

/******************************************** 
/**                                        **
/**                                        **
/**                                        **
/********************************************/
// Setup a local proxy to adjust the paths of requested files when loading them from the local production bundle (e.g.: local fonts, etc...).
function setupLocalFilesNormalizerProxy() {
    protocol.registerHttpProtocol(
        "file",
        (request, callback) => {
            const url = request.url.substr(8);
            callback({ path: path.normalize(`${__dirname}/${url}`) });
        },
        (error) => {
            if (error) console.error("Failed to register protocol");
        },
    );
}
/******************************************** 
/**                                        **
/**                                        **
/**                                        **
/********************************************/
// This method will be called when Electron has finished its initialization and is ready to create the browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
    createSplashWindow();
    setTimeout(() => {
        splashWindow.close();
        createWindow();
        setupLocalFilesNormalizerProxy();
    }, 1000);

    app.on("activate", function () {
        // On macOS it's common to re-create a window in the app when the
        // dock icon is clicked and there are no other windows open.
        if (BrowserWindow.getAllWindows().length === 0) {
            createWindow();
        }
    });
});

// Quit when all windows are closed, except on macOS.
// There, it's common for applications and their menu bar to stay active until the user quits  explicitly with Cmd + Q.
app.on("window-all-closed", function () {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

// If your app has no need to navigate or only needs to navigate to known pages, it is a good idea to limit navigation outright to that known scope, disallowing any other kinds of navigation.
const allowedNavigationDestinations = ["https://my-electron-app.com"];
app.on("web-contents-created", (event, contents) => {
    contents.on("will-navigate", (event, navigationUrl) => {
        const parsedUrl = new URL(navigationUrl);

        // if (!allowedNavigationDestinations.includes(parsedUrl.origin)) {
        //     event.preventDefault();
        // }
    });
});


// https://stackoverflow.com/questions/76324025/electron-how-to-fetch-a-file-from-a-local-path-and-create-a-blob-file