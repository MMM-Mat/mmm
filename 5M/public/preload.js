// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
const { contextBridge, ipcRenderer } = require("electron");
// ipcRenderer.invoke("showDialog", "new message");

// White-listed channels.
const ipc = {
    'render': {
        // From render to main.
        'send': [],
        // From main to render.
        'receive': [],
        // From render to main and back again.
        'sendReceive': [
            'dialog:openMultiFileSelect' // Channel name
        ]
    }
};


// Exposed protected methods in the render process.
contextBridge.exposeInMainWorld(
    // Allowed 'ipcRenderer' methods.
    'ipcRender', {
        // From render to main.
        send: (channel, args) => {
            let validChannels = ipc.render.send;
            if (validChannels.includes(channel)) {
                ipcRenderer.send(channel, args);
            }
        },
        // From main to render.
        receive: (channel, listener) => {
            let validChannels = ipc.render.receive;
            if (validChannels.includes(channel)) {
                // Deliberately strip event as it includes `sender`.
                ipcRenderer.on(channel, (event, ...args) => listener(...args));
            }
        },
        // From render to main and back again.
        invoke: (channel, args) => {
            let validChannels = ipc.render.sendReceive;
            if (validChannels.includes(channel)) {
                return ipcRenderer.invoke(channel, args);
            }
        }
    }
);

// As an example, here we use the exposeInMainWorld API to expose the browsers
// and node versions to the main window.
// They'll be accessible at "window.versions".
// process.once("loaded", () => {
//   contextBridge.exposeInMainWorld("versions", process.versions);
// });

contextBridge.exposeInMainWorld('electronAPI', {
    setTitle: (title) => ipcRenderer.send('set-title', title),
    
    openFile: (filepath) => ipcRenderer.invoke('openFile',filepath),
    
    startProgram: (pgmname) => ipcRenderer.invoke('startProgram',pgmname),

    trashFile: (filepath) => ipcRenderer.invoke('trashFile',filepath),

    encodeImage: (filepath) => ipcRenderer.invoke('encodeImage',filepath),
    
    pickFile: () => ipcRenderer.invoke('dialog:pickFile'),

    pickImage: () => ipcRenderer.invoke('dialog:pickImage'),
    
    showFileFolder : (filepath) => ipcRenderer.invoke('showFileFolder',filepath),
  })

 